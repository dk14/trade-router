package core

import com.hazelcast.core.{Hazelcast, HazelcastInstance}
import routing._
import routing.impl.DefaultDispatcher

import scalaz.concurrent.Task

object CoreApp extends App with ScriptRegistry with InterProcess {

   lazy val deps = Deps(DefaultDispatcher.hazel, DefaultDispatcher)

   lazy val injector = new Injector[Deps] {
      override def inject[T](handler: Deps => T): T = handler(deps)
   }

   Injectors.register("default", injector)

   RegistryHolder.loader = getClass.getClassLoader

   RegistryHolder.start()

   InterProcessMessaging.start()

   runAsyncScript("components/Component.scala")

   runAsyncScript("components/Coordination.scala")

   runAsyncScript("components/Console.scala")
}
