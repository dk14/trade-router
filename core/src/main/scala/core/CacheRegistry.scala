package core

import routing.Helpers._
import routing._
import routing.MultiCache._
import scala.reflect.runtime.universe._

/**
 * Created by user on 11/18/15.
 */
trait CacheRegistry {
  val deps: Deps
  import deps.multicache._

  type IoString = Event[IoMessage[String]]

  trait bus extends CacheName
  trait fpml extends CacheName
  trait fpmlAmend extends CacheName
  trait matchingExecCache extends CacheName
  trait allocations extends CacheName
  trait output extends CacheName
  trait output2 extends CacheName


  val busId = cacheId[IoString, Io, bus]
  val outputId = cacheId[IoString, Io, output]
  val output2Id = cacheId[IoString, Io, output2]
  val matchingExecCacheId = cacheId[ExecBase, PreferForMatching, matchingExecCache]
  val allocationsCacheId = cacheId[TradeNew, PreferForMatching, allocations]
  val fpmlId = cacheId[IoString, Io, fpml]
  val fpmlAmendId = cacheId[IoString, Io, fpmlAmend]

}

case class CacheRegistryImpl(deps: Deps) extends CacheRegistry

case object registry{
  val default = injections.injector.inject(CacheRegistryImpl)
}
