/**
 * Created by user on 11/7/15.
 */

import core.CacheRegistry

import scalaz._, Scalaz._
import routing.Helpers._
import routing._
import QueryModel._
import MultiCache._
import scalaz.stream.Process._

trait InputFlowAmend extends Flow[InputEvent[String], Unit] with  IO with ScriptRegistry with LazyQuery with CacheRegistry{

  import deps.multicache._

  def flowId = "amend flow"

  type NewAndAmend = (ExecNew, ExecAmend)

  def from = obtain[InputEvent[String] => ExecAmend]("transformers/FromFpmlAmend.scala")
  def to = obtain[ExecAmend => OutputEvent[String]]("transformers/ToFpml.scala")

  def mergeWithAmend = obtainProcess[NewAndAmend, ExecAmend]("transformers/Merge.scala")

  def sendAmend = sink(busId) contramap to

  import deps.multicache._


  def findEventNew(e: ExecAmend) = {
    val crId = prop[ExecBase, String](_.correlationId)
    val q = crId === e.msg.trade.event.linkId

    def checkCandidates(candidates: Set[ExecBase]) = if (candidates.size == 1) {
      val newE = candidates.flatMap(typeCheck[ExecutionAdvice[New], New].toOpt(_)).head
      emit(newE -> e)
    } else halt

    matching(matchingExecCacheId, q)(_.flatMap(checkCandidates))
  }

  def flow = (_
    map from
    flatMap findEventNew
    flatMap mergeWithAmend
    to sendAmend
  )

}

case class InputFlowAmendImpl(deps: Deps) extends InputFlowAmend

injections.injector.inject(InputFlowAmendImpl)


