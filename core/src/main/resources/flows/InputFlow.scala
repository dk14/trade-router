/**
 * Created by user on 11/7/15.
 */

import com.hazelcast.core.HazelcastInstance
import core.CacheRegistry
import routing.Helpers._
import routing.MultiCache._
import routing._

import com.softwaremill.quicklens._

trait InputFlow extends Flow[InputEvent[String], Unit] with  IO with ScriptRegistry with CacheRegistry {

  import deps.multicache._

  def flowId = "input flow"

  def from = obtain[InputEvent[String] => ExecNew]("transformers/FromFpml.scala")
  def to = obtain[ExecNew => OutputEvent[String]]("transformers/ToFpml.scala")

  def enrichToDraft = obtainProcess[ExecNew, ExecNew]("transformers/Draft.scala")
  def enrichToFinal = obtainProcess[ExecNew, ExecNew]("transformers/Final.scala")

  def sendDraft = sink(busId, "draft") contramap to
  def sendFinal = sink(busId, "final") contramap to

  def flow = (_
    map {x => println(x); x}
    map from
    flatMap enrichToDraft
    observe snapshot("draft")
    observe sendDraft
    flatMap enrichToFinal
    observe snapshot("final")
    observe sendFinal
    to snapshot("fpml", matchingExecCacheId)
  )

}

case class InputFlowImpl(deps: Deps) extends InputFlow

injections.injector.inject(InputFlowImpl)


