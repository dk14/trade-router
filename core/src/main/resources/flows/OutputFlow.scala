/**
 * Created by user on 11/8/15.
 */

import com.hazelcast.core.HazelcastInstance
import core.CacheRegistry
import routing.Helpers._
import routing._

import scalaz.concurrent.Task
import scalaz.stream.Process._
import scalaz.stream.Process0

trait OutputFlow extends Flow[InputEvent[String], Unit] with IO with ScriptRegistry with CacheRegistry {

  import deps.multicache._

  def flowId = "output flow"

  def from = obtain[InputEvent[String] => ExecBase]("transformers/FromFpml.scala")
  def to = obtain[ExecBase => OutputEvent[String]]("transformers/ToFpml.scala")
  def to2 = obtain[ExecBase => OutputEvent[String]]("transformers/ToFpml.scala")

  def send = sink(outputId) contramap to
  def send2 = sink(output2Id) contramap to2


  def amendFlow: P[ExecBase] => P[Unit] = (_
    flatMap typeCheck[ExecutionAdvice[Amend], Amend].toProcess
    observe send
    to snapshot("amend")
    onFailure specify("amend flow"))

  def newFlow: P[ExecBase] => P[Unit] = (_
    flatMap typeCheck[ExecutionAdvice[New], New].toProcess
    observe send
    to snapshot("new")
    onFailure specify("new flow"))

  def parallellFlow: P[ExecBase] => P[Unit] = (_
    observe send2
    to snapshot("flow3")
    onFailure specify("parallell flow"))

  def flow = (_
    map from
    map emit
    flatMap(p => amendFlow(p) merge newFlow(p) merge parallellFlow(p))
  )

}

case class OutputFlowImpl(deps: Deps) extends OutputFlow

injections.injector.inject(OutputFlowImpl)


