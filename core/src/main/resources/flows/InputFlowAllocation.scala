import com.hazelcast.core.HazelcastInstance
import core.CacheRegistry
import routing.Helpers._
import routing._
import scalaz.stream._
import com.softwaremill.quicklens._
import QueryModel._
import routing.MultiCache._

trait InputFlowAllocation extends Flow[InputEvent[String], Unit] with  IO with ScriptRegistry with CacheRegistry {

  import deps.multicache._

  def flowId = "allocation flow"

  def from = obtain[InputEvent[String] => ExecNew]("transformers/FromFpml.scala")
  def to = obtain[ExecNew => OutputEvent[String]]("transformers/ToFpml.scala")

  def sendDraft = sink(busId, "draft") contramap to


  val q = implicitly[QueryableWithoutKey[TradeNew, PreferForSnapsots, allocations]] //TODO move it to dsl
  val allocId = prop[TradeNew, String](_.header.allocationId.get)

  def lastAllocation(e: ExecNew) = {
    val neighbours = q.query(allocationsCacheId, allocId === e.msg.trade.header.allocationId.get)
    val sum = neighbours.map(_.event.notional3).sum
    if (sum > 100500) Process.emit(e.modify(_.msg.trade.event.notional3).setTo(sum)) else Process.halt
  }

  def sendTrade = sink(busId) contramap to

  def flow = (_
    map from
    observe projection("candidate", allocationsCacheId)(_.msg.trade) //TODO simplfy `projection` method to make IDEA happy; it compiles with sbt actually
    flatMap lastAllocation
    to sendTrade
  )

}

case class InputFlowAllocationImpl(deps: Deps) extends InputFlowAllocation

injections.injector.inject(InputFlowAllocationImpl)
