/**
 * Created by user on 11/7/15.
 */

import com.hazelcast.core.HazelcastInstance
import routing._
import Helpers._
import com.softwaremill.quicklens._
import services._
import scalaz._, Scalaz._

import scalaz.concurrent.Task
import scalaz.stream._

trait Final extends TransformWithTask[ExecNew, ExecNew] with Arithmetic {

  def transformWithTask = t => {
    val sum1F = plus(t.msg.trade.event.notional1, t.msg.trade.event.notional2)
    val sum2F = plus(t.msg.trade.event.notional1, t.msg.trade.event.notional3)

    for {
      l <- Task gatherUnordered List(sum1F, sum2F) //non-deterministic combinator
      List(sum1, sum2) = l
      sum <- plus(sum1, sum2)
      notional <- mul(sum, t.msg.trade.event.notional3)
    } yield {
      t
        .modify(_.msg.trade.header.ids.each.id).using(_ + "100500")
        .modify(_.msg.trade.event.notional).setTo(notional.some)
        .modify(_.name).setTo("final")
    }
  }
}

case class FinalImpl(deps: Deps) extends Final  with CurrentArithmeticImpl
injections.injector.inject(FinalImpl)
