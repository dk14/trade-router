/**
 * Created by user on 11/7/15.
 */

import com.hazelcast.core.HazelcastInstance
import org.apache.commons.io.IOUtils
import routing._
import scala.xml._


trait FromFpmlAmend extends Transformer[Event[IoMessage[String]], Event[TradeMessage[ExecutionAdvice[Amend]]]] {
  def transform = e => {
    val xml = XML.load(IOUtils.toInputStream(e.msg.raw, "UTF-8"))
    e.copy(
      msg = TradeMessage (
        messageId = (xml \ "header" \ "messageId").text,
        trade = ExecutionAdvice(
           header = TradeHeader(
             ids = (xml \ "trade" \ "tradeHeader" \ "partyTradeIdentifier") map { x =>
               TradeId(Party(x \"partyReference" \@ "href", ""), (x \"versionedTradeId" \ "tradeId").text)
             } toList
           ),
           event = Amend("CORR002")
        )
      )
    )
  }
}

case class FromFpmlAmendImpl(deps: Deps) extends FromFpmlAmend
injections.injector.inject(FromFpmlAmendImpl)
