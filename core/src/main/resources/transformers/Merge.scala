/**
 * Created by user on 11/7/15.
 */

import com.hazelcast.core.HazelcastInstance
import routing._
import Helpers._
import com.softwaremill.quicklens._

import scalaz.concurrent.Task
import scalaz.stream._
import scalaz.concurrent.Task._
import scalaz.stream.Process._

trait Merge extends TransformWithProcess[(ExecNew, ExecAmend), ExecAmend] {
  def transformWithProcess =
    _._2.modify(_.msg.trade.header.ids.each.id).using(_ + "100500")

}

case class MergeImpl(deps: Deps) extends Merge
injections.injector.inject(MergeImpl)
