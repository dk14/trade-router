/**
 * Created by user on 11/7/15.
 */

import java.io.InputStream

import com.hazelcast.core.HazelcastInstance
import org.apache.commons.io.IOUtils
import routing._
import scala.xml._


trait FromFpml extends Transformer[Event[IoMessage[String]], Event[TradeMessage[ExecutionAdvice[New]]]] {
  def transform = e => {
    val xml = XML.load(IOUtils.toInputStream(e.msg.raw, "UTF-8"))
    e.copy(
      msg = TradeMessage (
        messageId = (xml \ "header" \ "messageId").text,
        trade = ExecutionAdvice(
          header = TradeHeader(
            ids = (xml \ "trade" \ "tradeHeader" \ "partyTradeIdentifier") map { x =>
              TradeId(Party(x \"partyReference" \@ "href", ""), (x \"versionedTradeId" \ "tradeId").text)
            } toList
          ),
          event = New()
        )
      )
    )
  }
}

case class FromFpmlImpl(deps: Deps) extends FromFpml
injections.injector.inject(FromFpmlImpl)
