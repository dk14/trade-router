/**
 * Created by user on 11/7/15.
 */

import com.hazelcast.core.HazelcastInstance
import routing._

trait ToFpml extends Transformer[Event[TradeMessage[ExecutionAdvice[TradeEvent]]], Event[IoMessage[String]]] {
  def transform = e => e.copy(msg = IoMessage(
    s"""<Fpml>
    ${e.msg.trade.header.ids.map{ id =>
      s"""<tradeId>${id}</tradeId>"""
    }}
  </Fpml>"""))

}

case class ToFpmlImpl(deps: Deps) extends ToFpml
injections.injector.inject(ToFpmlImpl)
