/**
 * Created by user on 11/7/15.
 */

import com.hazelcast.core.HazelcastInstance
import routing._
import Helpers._
import com.softwaremill.quicklens._
import scalaz._, Scalaz._
import scalaz.stream._



import scalaz.concurrent.Task

trait Draft extends TransformWithProcess[ExecNew, ExecNew]{

  def transformWithProcess = _
    .modify(_.msg.trade.header.ids.each.id).using(_ + "100500")
    .modify(_.msg.trade.event.notional).setTo(200.some)
    .modify(_.name).setTo("draft")

}

case class DraftImpl(deps: Deps) extends Draft
injections.injector.inject(DraftImpl)
