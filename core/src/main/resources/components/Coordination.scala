import routing.coordination.default._
import scalaz._, Scalaz._

mutex("orchestration") {

  watchdog[String]("watchdog"){ services =>
    //notify nodes if something dies and maybe restart died service/component on reserved bundle
  }

  "component1.bundle" ! "default"

  "component1.init".await

  "preloading" ! "Preloaded"

  "component1.command" ! "start"

  "orchestration.stop".await

}
