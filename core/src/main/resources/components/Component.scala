import routing._
import routing.injections.common._
import routing.coordination.default._
import routing.coordination._
import scalaz._, Scalaz._
import \/._
import Exit._
import Helpers._
import core.registry.default._
import routing.injections.common.deps.multicache._

val endpoints = List(
  endpoint(fpmlId)(obtain[Flow[InputEvent[String], Unit]]("flows/InputFlow.scala")),
  endpoint(fpmlAmendId)(obtain[Flow[InputEvent[String], Unit]]("flows/InputFlowAmend.scala")),
  endpoint(busId)(obtain[Flow[InputEvent[String], Unit]]("flows/OutputFlow.scala"))
)

checkBundleFor("component1")

val currentThread = Thread.currentThread()
on[String]("component1.kill9"){_ => currentThread.interrupt()}

println(s"I'm in $currentBundle bundle!")

mutex("component1") {

  "component1.init" ! "OK"

  "preloading".await

  on("preloaded.data.requireHalt") { cause =>
    "component1.command" :-| cause
    "preloaded.data.readyAgain".await
    "component1.command" ! "start!"
  }

  val commands = Iterator.continually(await[String]("component1.command"))

  commands.foldLeft(Init: ProcessState){
    case (_: StartIsFine, \/-("start")) =>
      fromTryCatchThrowable[String, Throwable] {

        println("Started")

        alive("component1", "Starting endpoints...".right)

        endpoints.foreach(_.start())

        alive("component1", "Endpoints have been started...".right)

        "OK"
      } ==> "component1.started"
      Started
    case (_: StopIsFine, \/-("stop")) =>
      fromTryCatchThrowable[String, Throwable] {
          endpoints.foreach(_.stop())
          "inacative"
      }.alive("component1") ==> "component1.stopped"

      Stopped
    case (_, \/-("kill")) =>
      suicide()
    case (_, -\/(cause)) =>
      alive("component1", cause.left)
      cause.printStackTrace()
      endpoints.foreach(_.stop())
      Stopped
    case (s, x) =>
      alive("component1", new RuntimeException("Incorrect state change").left)
      println(x)
      s
  }
}
