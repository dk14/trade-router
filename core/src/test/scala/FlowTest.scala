/*

import core.CoreApp
import org.scalatest._
import routing._

import scala.io.Source
import scalaz.stream.Process._
import scala.collection.JavaConverters._
import com.hazelcast.test._

class FlowTest extends FunSuite with IO with Registry with BeforeAndAfterAll {

  lazy val deps = Deps(new TestHazelcastInstanceFactory().newHazelcastInstance())

  lazy val injector = new Injector[Deps] {
    override def inject[T](handler: (Deps) => T): T = handler(deps)
  }

  Injectors.register("default", injector)

  RegistryHolder.loader = CoreApp.getClass.getClassLoader

  test("input flow ") {
    val flow = obtain[Flow[String]]("flows/InputFlow.scala")
    val data = Source.fromInputStream(getClass.getResourceAsStream("input/execAdvice.xml")).mkString
    val e = Event("CORR002", IoMessage[String](data), "inbound", 0)

    println("input" + flow(emit(e)).run.run)

  }

  test("output flow ") {
    val flow = obtain[Flow[String]]("flows/OutputFlow.scala")
    val data = Source.fromInputStream(getClass.getResourceAsStream("input/execAdvice.xml")).mkString
    val e = Event("", IoMessage[String](data), "inbound", 0)

    println("output" + flow(emit(e)).run.run)

  }

  test("matching flow ") {
    val flow = obtain[Flow[String]]("flows/InputFlowAmend.scala")
    val data = Source.fromInputStream(getClass.getResourceAsStream("input/execAdvice.xml")).mkString
    val e = Event("", IoMessage[String](data), "inbound", 0)

    println("matching" + flow(emit(e)).run.run)
  }

  override def afterAll: Unit = {
    deps.hazel.shutdown()
  }
}
*/
