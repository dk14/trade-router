/**
 * Created by user on 11/8/15.
 */

import org.scalatest._

import scala.io.Source

class ScriptsTest extends FunSuite {

  import scala.reflect.runtime._
  import scala.tools.reflect.ToolBox
  private val cm = universe.runtimeMirror(getClass.getClassLoader)
  private val tb = cm.mkToolBox()


  val sources = List("components/Coordination", "components/Component", "flows/InputFlow", "flows/InputFlowAmend", "flows/InputFlowAllocation", "flows/OutputFlow",
    "transformers/Draft", "transformers/Final", "transformers/FromFpml", "transformers/ToFpml", "transformers/FromFpmlAmend", "transformers/Merge")

  sources.map { name =>
    test("compile " + name) {
      val data = Source.fromInputStream(getClass.getResourceAsStream(name + ".scala")).mkString
      val parsed = tb.parse(data)
      tb.compile(parsed)

    }
  }

}
