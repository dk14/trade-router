package macros
import scala.reflect.macros.whitebox.Context
import scala.language.experimental.macros

trait PropsMacro extends WithProp {
  def prop[T, U](extr: T => U): Prop[T] = macro PropsMacroImpl.impl[T, U]
}

object PropsMacroImpl extends WithProp {
  def impl[T: c.WeakTypeTag, U: c.WeakTypeTag](c: Context)(extr: c.Expr[T => U]) = {
    import c.universe._
    val res = """.*\$.*\.(.*?)\)""".r.findFirstMatchIn(extr.tree.toString).get.group(1) //TODO analise AST instead
    q"Prop[${weakTypeOf[T]}]($res)"
  }
}