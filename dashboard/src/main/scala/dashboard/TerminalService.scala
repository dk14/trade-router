package dashboard

import java.util.logging.{Level, Logger}

import akka.actor.{Actor, ActorRefFactory}
import com.hazelcast.core
import com.hazelcast.core.{Hazelcast, MessageListener, HazelcastInstance}
import dashboard.TerminalService
import routing._
import routing.impl.DefaultDispatcher

import spray.http.MediaTypes._
import spray.http.{StatusCodes}
import spray.routing.HttpService

import scala.concurrent._
import scala.util.{Failure, Success}
import scala.collection.JavaConverters._
import macros._
import QueryModel._

// this trait defines our service behavior independently from the service actor
trait TerminalService extends HttpService with Compilation with Reprocessing with Events {
  import spray.routing._
  import directives._
  import RouteDirectives._
  import OnCompleteFutureMagnet._

  implicit val actorRefFactory: ActorRefFactory

  import scala.concurrent.ExecutionContext.Implicits.global

  val myRoute =
    path("dashboard" / "ui") {
      getFromResource("WEB-INF/index.html")
    } ~ pathPrefix("dashboard" / "ui") {
        getFromResourceDirectory("WEB-INF")
    } ~ path("dashboard" / "scripts" / "list" ) {
      get & complete(listScripts)
    } ~ path("dashboard" / "scripts" / "get" / Segment) { name =>
      get & complete(getScript(name))
    } ~ path("dashboard" / "scripts" / "update" / Segment ) { name =>
      post {
        respondWithMediaType(`application/json`) {
          entity(as[String]) { req =>
            onComplete(getCompilationResult(name, req)) {
              case Success(r) => complete(r)
              case Failure(e) => complete(StatusCodes.BadRequest, e.getMessage)
            }
          }
        }
      }
    } ~ path("dashboard" / "events" / "list") {
      get & complete(list)
    } ~ path("dashboard" / "events" / "content" / Segment) { id =>
      get & complete(content(id))
    } ~ path("dashboard" / "events" / "reprocess" / Segment) { id =>
      post & complete(reprocess(id))
    }
}


trait Compilation {

  val logger = Logger.getLogger("dashboard")


  def deps: Deps
  private lazy val cache = deps.hazel.getMap[String, Script]("scripts")
  private lazy val compilationResult = deps.hazel.getTopic[Response]("compiled")

  def getCompilationResult(name: String, data: String): Future[String] = {
    val result = Promise[String]
    val s = Script(data)
    cache.put(name, s)

    object Listeners {
      lazy val id: String = compilationResult.addMessageListener(listener)

      lazy val listener: MessageListener[Response] = new MessageListener[Response]{
        override def onMessage(message: core.Message[Response]): Unit = {
          if(message.getMessageObject.correlationId == s.correlationId) {
            result.success(message.getMessageObject.result)
            compilationResult.removeMessageListener(id)
          }
        }
      }

    }

    val listenerId = Listeners.id
    logger.log(Level.INFO, s"registered listener with id=${listenerId}") //TODO use normal logger
    result.future
  }


  import scala.collection.JavaConverters._

  def listScripts: String = cache.keySet().asScala.toList.sorted.mkString("\n")


  def getScript(name: String) = cache.get(name).data


}

trait Reprocessing extends Cache {

  def deps: Deps

  private lazy val cache = deps.hazel.getMap[Long, Event[Message]]("events")

  type M = Event[IoMessage[_]]
  val cid = prop[M,String](_.correlationId)
  val sta = prop[M,String](_.status)

  def reprocess(corrId: String): String = {
    val toReprocess = queryAll("events", cid === corrId && sta === "[received]").head
    val to = toReprocess.asInstanceOf[M].msg.endpoint
    deps.hazel.getQueue[Event[_]](to).put(toReprocess)
    "OK"
  }

  def forceSend(corrId: String): String = {
    val toReprocess = queryAll("events", cid === corrId && sta === "[sent]").head
    val to = toReprocess.asInstanceOf[M].msg.endpoint
    deps.hazel.getQueue[Event[_]](to).put(toReprocess)
    "OK"
  }
}

trait Events {

  def deps: Deps

  private lazy val cache = deps.hazel.getMap[Long, Event[Message]]("events")

  case class MessageUI(events: List[EventUI])
  case class EventUI(eventId: Long, corrId: String, name: String, status: String, isOutput: Boolean, content: Option[String] = None, destination: Option[String] = None)

  def list: String = {

    def symb(isIncoming: Boolean) = if (isIncoming) "==>" else "<=="
    def fromTo(isIncoming: Boolean) = if (isIncoming) "FROM" else "TO"

    val extract: Event[Message] => String = {
      case Event(corrId, m: IoMessage[_], name, id, _, _, status) => f"${symb(m.isIncoming)} $status $corrId:$id $name ${fromTo(m.isIncoming)} ${m.endpoint}"
      case Event(corrId, _, name, id, _, _, status) => f"=== $status $corrId:$id $name"
    }

    cache.values().asScala.toList
      .groupBy(_.correlationId).toList.sortBy(_._1)
      .map(_._2.sortBy(_.id).map(extract).reverse.mkString("\n")).reverse.mkString("\n\n")
  }

  def content(id: String) = cache.get(id.toLong).toString

}


class ServiceActor extends Actor with TerminalService {

  // the HttpService trait defines only one abstract member, which
  // connects the services environment to the enclosing actor or test
  lazy val actorRefFactory = context
  import akka.util._
  import scala.concurrent.duration._
  import context.dispatcher


  val deps = Deps(DefaultDispatcher.hazel, DefaultDispatcher)

  def receive = runRoute(myRoute)

}

trait Cache {

  def deps: Deps

  def queryAll[T](cache: String, p: Pred[T]): Seq[T] = deps.hazel.getMap[Any, T](cache).values(p.asHazel).asScala.toSeq //TODO typesafe cache name

}