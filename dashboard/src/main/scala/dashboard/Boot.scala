package dashboard

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import macros.PropsMacro
import routing.MultiCache
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import macros._

object Boot extends App with PropsMacro {

  case class A(v: B)
  case class B(v: A)

  println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + prop[B, A](_.v.v.v.v.v))

  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("on-spray-can")

  // create and start our service actor
  val service = system.actorOf(Props(classOf[ServiceActor]), "dashboard")


  implicit val timeout = Timeout(5.seconds)
  // start a new HTTP server on port 8080 with our service actor as the handler
  IO(Http) ? Http.Bind(service, interface = "localhost", port = 8080)

}
