declare var editor: any

var currentScript = ""

const listScripts = () => {
    const x = new XMLHttpRequest() //need this low-level api to obtain modern "responseURL" header ($http doesn't provide it)
    x.open("GET", "../scripts/list", true)
    x.onreadystatechange = () => {
      if (x.readyState == 4 && x.status == 200) {
        const text = x.responseText.split('\n').map(x => {return `<a href="#" class="list-group-item" onClick="loadScript('${x}')">${x}</a>`}).join('\n')
        document.getElementById("scripts").innerHTML = text
      }
    }
    x.send()
}

const listEvents = () => {
    const x = new XMLHttpRequest() //need this low-level api to obtain modern "responseURL" header ($http doesn't provide it)
    x.open("GET", "../events/list", true)
    x.onreadystatechange = () => {
      if (x.readyState == 4 && x.status == 200) {
        document.getElementById("events").innerHTML = `<pre>${x.responseText}</pre>`
      }
    }
    x.send()
}


const loadScript = name => {
   const x = new XMLHttpRequest() //need this low-level api to obtain modern "responseURL" header ($http doesn't provide it)
   x.open("GET", "../scripts/get/" + name.replace('/', '%2F'), true)
   x.onreadystatechange = () => {
     if (x.readyState == 4 && x.status == 200) {
        editor.getDoc().setValue(x.responseText)
        document.getElementById("savedata").innerHTML = `<a href="#" onClick="saveScript('${name}')">Save</a>`
        currentScript = name
     }
   }
   x.send()
}

const saveScript = name => {
   const x = new XMLHttpRequest() //need this low-level api to obtain modern "responseURL" header ($http doesn't provide it)
   x.open("POST", "../scripts/update/" + name.replace('/', '%2F'), true)
   x.onreadystatechange = () => {
     if (x.readyState == 4 && x.status == 200) {
        console.log(x.responseText)
        loadScript(name)
     }
   }
   x.send(editor.getDoc().getValue())
   console.log("Sent!")

}

const handler = () => {
  listScripts()
  listEvents()
}

handler()
window.setInterval(handler, 1000)

function saveTextHandler(ev) {saveScript(currentScript)}

window.addEventListener("saveText", saveTextHandler, false);