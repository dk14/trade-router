function fireEvent(name, target, param) {
    //Ready: create a generic event
    var evt = document.createEvent("Events")
    //Aim: initialize it to be the event we want
    evt.initEvent(name, true, true); //true for can bubble, true for cancelable
    evt.param = param;
    //FIRE!
    target.dispatchEvent(evt);
}



var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
  lineNumbers: true,
  styleActiveLine: true,
  matchBrackets: true,
  extraKeys: { "Cmd-S": function(instance) { fireEvent("saveText", document, instance); }}
});

editor.setOption("theme", "lesser-dark")
editor.setOption("mode", "text/x-scala")

$('.cd').height(800)

editor.setSize($('.codepanl').width(), $('.codepanl').height());
$('.scrollable-menu').css('max-height', $('.cd').height() + 'px');
$('.scrollable-menu').css('overflow-x', 'hidden');

$(document).ready(function() {

  $(window).bind("resize", function(event) {
  	if (this == event.target) {
      $('.sp').removeAttr('style');
    }
  });


  $('.editor').resizable({
     handles: 'e, s',
	 minWidth:100,
   	 maxWidth:900,
  	 resize:function(event,ui){

    	var x=ui.element.outerWidth();
    	var y=ui.element.outerHeight();
    	var ele=ui.element;
     	var factor = $(this).parent().width()-x;
      	var f2 = $(this).parent().width() * .02999;
      	editor.setSize($('.codepanl').width(), $('.codepanl').height());
      	$('.scrollable-menu').css('max-height', $('.cd').height() + 'px');
      	console.log(f2);
      	$.each(ele.siblings(),function(idx,item){

        	ele.siblings().eq(idx).css('height',y+'px');
          	//ele.siblings().eq(idx).css('width',(factor-41)+'px');
        	ele.siblings().eq(idx).width((factor-f2)+'px');

      	});
     }
  });

  $('.sp:not(.editor)').resizable({
     handles: 's',
  	 resize:function(event,ui){

    	var y=ui.element.outerHeight();
    	var ele=ui.element;

      	$.each(ele.siblings(),function(idx,item){
        	ele.siblings().eq(idx).css('height',y+'px');
      	});

      	$('.scrollable-menu').css('max-height', $('.cd').height() + 'px');
      	editor.setSize($('.codepanl').width(), $('.codepanl').height());
     }
  });

});
        
