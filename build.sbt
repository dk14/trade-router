
val sets = Seq(
  scalaVersion := "2.11.8-SNAPSHOT",
  resolvers ++= Seq(
    "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/",
    DefaultMavenRepository,
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  ),
  libraryDependencies ++= Seq(
    "com.softwaremill.quicklens" %% "quicklens" % "1.4.2",
    "commons-io" % "commons-io" % "2.4",
    "org.apache.commons" % "commons-lang3" % "3.4",
    "org.scalaz" %% "scalaz-core" % "7.1.0",
    "org.scalaz.stream" %% "scalaz-stream" % "0.7.2a",
    "com.hazelcast" % "hazelcast" % "3.5.3",
    "com.hazelcast" % "hazelcast" % "3.5.3" % "test" classifier "tests",
    "org.scala-lang" % "scala-compiler" % scalaVersion.value,
    "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
    "org.bluestemsoftware.open.maven.tparty" % "xerces-impl" % "2.9.0"
  )
)

val dashboardSets = sets ++ Seq(
  libraryDependencies ++= {
    val akkaV = "2.3.6"
    val sprayV = "1.3.2"
    Seq(
      "io.spray"            %%  "spray-can"     % sprayV,
      "io.spray"            %%  "spray-client"  % sprayV,
      "io.spray"            %%  "spray-routing" % sprayV,
      "io.spray"            %%  "spray-json"    % "1.3.1",
      "io.spray"            %%  "spray-testkit" % sprayV  % "test",
      "com.typesafe.akka"   %%  "akka-actor"    % akkaV,
      "com.typesafe.akka"   %%  "akka-agent"    % akkaV,
      "com.typesafe.akka"   %%  "akka-testkit"  % akkaV   % "test",
      "org.specs2"          %%  "specs2-core"   % "2.3.11" % "test"
    )
  }
)

lazy val root = project.in(file(".")).aggregate(common, core, services, dashboard, model)

lazy val macros = project in file ("macro")  settings(sets:_*)

lazy val common = project in file("common") dependsOn macros settings(sets:_*)

lazy val core = project in file ("core") dependsOn (common, services, macros) settings(sets:_*)

lazy val services = project in file ("services") dependsOn common settings(sets:_*)

lazy val dashboard = project in file ("dashboard") dependsOn (common, macros) settings((sets ++ dashboardSets ++ Revolver.settings):_*)

lazy val transport = project in file ("transport") dependsOn common settings(sets:_*)

lazy val model = project in file ("model") dependsOn common settings(sets:_*)
