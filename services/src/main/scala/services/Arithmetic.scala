package services

import java.util.concurrent.Executors

import com.hazelcast.core.HazelcastInstance
import routing.Deps


import scala.concurrent.ExecutionContext
import scalaz.\/
import scalaz.concurrent._
import scalaz.stream.Process
import scalaz.concurrent.Task._
import scalaz.stream.Process._

trait Arithmetic {

  def plus(a: Int, b: Int): Task[Int]
  def mul(a: Int, b: Int): Task[Int]

}

trait CurrentArithmeticImpl extends ArithmeticImpl //choose current impl

trait ArithmeticImpl extends Arithmetic {

  def deps: Deps //let's say we need it here

  def plus(a: Int, b: Int) = delay(a + b) //we don't need any pool here, it's gonna br executed in caller's pool
  def mul(a: Int, b: Int) = delay(a * b)

}



trait SomeOldServiceImpl extends Arithmetic {

  implicit val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(20))

  def plus(a: Int, b: Int) = Task {
    a + b
  }


  def mul(a: Int, b: Int) = Task {
    a * b
  }

}

import scalaz._, Scalaz._

trait SomeAsyncApiImpl extends Arithmetic {

  implicit val ec = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(20))

  def registerSumListener(a: Int, b: Int, sum: Int => Unit, error: Throwable => Unit): Unit = sum(a + b)
  def registerMulListener(a: Int, b: Int, sum: Int => Unit, error: Throwable => Unit): Unit = sum(a * b)

  def plus(a: Int, b: Int) = Task.async { cb: (Throwable \/ Int => Unit) =>
      registerSumListener(a, b , x => cb(x.right), t => cb(t.left))
  }

  def mul(a: Int, b: Int) = Task.async { cb: (Throwable \/ Int => Unit) =>
      registerMulListener(a, b , x => cb(x.right), t => cb(t.left))
  }

}


