package routing

import routing.MultiCache._

trait CommonCacheRegistry {
  val deps: Deps
  import deps.multicache._

  trait events extends CacheName
  def eventsCacheId[T <: Message] = cacheId[Event[T], PreferForSnapsots, events]

}
