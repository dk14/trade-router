package routing

import com.hazelcast.query.{Predicates, Predicate}
import macros.PropsMacro

import scala.annotation.implicitNotFound
import scala.concurrent.duration.Duration

import scala.language.experimental.macros
import scalaz.stream.async.mutable._
import scalaz._, Scalaz._
import scala.reflect.runtime.universe._

object MultiCache extends MultiCache

trait MultiCache {
  import QueryModel._

  import Helpers.P

  trait CacheName //EventCache, MatchingCache etc.
  trait DynamicName extends CacheName

  trait CacheType //Coherence, Zookeeper, Hazelcast etc.

  trait CacheId[+EntryT, +CacheT <: CacheType, +Name <: CacheName]{
    def name: String
  }

  def cacheId[EntryT, CacheT <: CacheType, Name <: CacheName: TypeTag] = new CacheId[EntryT, CacheT, Name]{
    val name = typeTag[Name].tpe.toString.replaceAll(".*\\.this\\.", "") //TODO analise AST instead
  }

  case class NonTypedId[EntryT, CacheT <: CacheType](override val name: String) extends CacheId[EntryT, CacheT, DynamicName]

  trait CacheEvent
  trait EventNew extends CacheEvent
  trait EventUpdate extends CacheEvent
  trait EventRemoved extends CacheEvent
  trait EventEvicted extends CacheEvent
  trait QueryResponse extends CacheEvent //one notification per entry
  trait RepeatableQueryResponse extends CacheEvent //combined notification with some entry and all values

  case class CacheNotification[T](event: CacheEvent, value: T, oldValue: Option[T] = none, queryResult: List[T] = nil)

  trait Operationable[EntryTyp, CacheTyp <: CacheType, NameTyp <: CacheName] {
    type EntryT = EntryTyp
    type CacheT = CacheTyp
    type Name = NameTyp
    type Id = CacheId[EntryTyp, CacheTyp, NameTyp]
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} can't query values of ${V}")
  trait Queryable[K, V, CacheT <: CacheType, Name <: CacheName] extends Operationable[(K,V), CacheT, Name] {
    def query(id: Id, p: Pred[V]): Seq[EntryT]
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} can't query (without key) values of ${V}")
  trait QueryableWithoutKey[V, CacheT <: CacheType, Name <: CacheName] { //TODO too verbose, just write it in MultiCache.dsl
    def query(id: CacheId[V, CacheT, Name], p: Pred[V])(implicit q: Queryable[Any, V, CacheT, Name]): Seq[V] = {
      q.query(id.asInstanceOf[CacheId[(Any,V), CacheT, Name]], p).map(_._2)
    }
  }
  
  @implicitNotFound(msg = "${Name} of type ${CacheT} can't read ${V} by key ${K}")
  trait Readable[K, V, CacheT <: CacheType, Name <: CacheName] extends Operationable[(K,V), CacheT, Name] {
    def read(id: Id, k: K): V
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} can't poll ${T}")
  trait Pollable[T, CacheT <: CacheType, Name <: CacheName] extends Operationable[T, CacheT, Name] {
    def poll(id: Id): EntryT
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} cannot subscribe (once) on ${CacheEventT} of ${T}") //TODO too verbose; write it separately
  trait SubscribableOnce[T, CacheT <: CacheType, Name <: CacheName, CacheEventT <: CacheEvent] extends Operationable[T, CacheT, Name]{
    //unsubscribes when handler emits a value; returned process contains that emitted value
    def subscribeOnce[U](id: Id, query: Pred[T])(handler: P[CacheNotification[T]] => P[U])(implicit q: Queryable[_, T, this.CacheT, this.Name]): P[U]

  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} cannot subscribe on ${CacheEventT} of ${T}")
  trait Subscribable[T, CacheT <: CacheType, Name <: CacheName, CacheEventT <: CacheEvent] extends Operationable[T, CacheT, Name]{

    //you should close topic after being used
    def subscribe(id: Id, query: Pred[T], handle: T => Unit = (_: T) => Unit): Topic[CacheNotification[T]]
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} can't push ${T}")
  trait Pushable[T, CacheT <: CacheType, Name <: CacheName] extends Operationable[T, CacheT, Name]{
    def push(id: Id, e: T, evictionTime: Duration): T
  }

  @implicitNotFound(msg = "${CacheName} of type ${CacheT} can't update ${K} with ${V}")
  trait Updatable[K, V, CacheT <: CacheType, Name <: CacheName] extends Operationable[(K,V), CacheT, Name]{
    def update(id: Id, k: K, v: V): Option[V]
  }

  @implicitNotFound(msg = "${CacheName} of type ${CacheT} can't generate id")
  trait Identifiable[CacheT <: CacheType, Name <: CacheName] extends Operationable[Any, CacheT, Name]{
    def genEntryId(id: Id): Long
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} can't remove ${T}")
  trait Removable[T, CacheT <: CacheType, Name <: CacheName]  extends Operationable[T, CacheT, Name]{
    def remove(id: Id, entry: EntryT): Throwable \/ EntryT
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} can't remove ${K}")
  trait RemovableByKey[K, V, CacheT <: CacheType, Name <: CacheName] extends Operationable[(K,V), CacheT, Name]{
    def removeByKey(id: Id, k: K): Throwable \/ EntryT
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} cannot be locked")
  trait Lockable[CacheT <: CacheType, Name <: CacheName] extends Operationable[Any, CacheT, Name] {
    def withLock[U](id: Id, freq: Duration, factor: Int)(handler: => U): U
  }

  @implicitNotFound(msg = "${Name} of type ${CacheT} can't update properties for ${V}")
  trait PropertyUpdatable[K, V, PropK, PropV, Properties[_, _], CacheT <: CacheType, Name <: CacheName] extends Operationable[(K,V), CacheT, Name] {
    def updateDynamicProperties(id: Id, k: K, properties: Properties[PropK, PropV])
  }


  import QueryModel._
  object dsl {

    import scala.concurrent.duration._

    def query[K, V, CacheT <: CacheType, Name <: CacheName](id: CacheId[(K, V), CacheT, Name], p: Pred[V])
      (implicit impl: Queryable[K, V,CacheT, Name]): Seq[(K, V)] = impl.query(id, p)

    def read[K, V, CacheT <: CacheType, Name <: CacheName](id: CacheId[(K,V), CacheT, Name], k: K)
      (implicit impl: Readable[K,V,CacheT, Name]): V = impl.read(id, k)

    def removeByKey[K, V, CacheT <: CacheType, Name <: CacheName](id: CacheId[(K,V), CacheT, Name], k: K)
      (implicit impl: RemovableByKey[K,V,CacheT, Name]): Throwable \/ (K,V) = impl.removeByKey(id, k)

    def subscribe[T, CacheT <: CacheType, Name <: CacheName, CacheEventT <: CacheEvent]
      (id: CacheId[T, CacheT, Name], query: Pred[T], handle: T => Unit = (_: T) => Unit)
      (implicit impl: Subscribable[T, CacheT, Name, CacheEventT]) = impl.subscribe(id, query, handle)

    def subscribeOnce[T, U, CacheT <: CacheType, Name <: CacheName, CacheEventT <: CacheEvent]
      (id: CacheId[T, CacheT, Name], query: Pred[T])(handler: P[CacheNotification[T]] => P[U])
      (implicit impl: SubscribableOnce[T, CacheT, Name, CacheEventT], impl2: Queryable[_, T,CacheT, Name]) = impl.subscribeOnce(id, query)(handler)

    def withLock[U, CacheT <: CacheType, Name <: CacheName]
      (id: CacheId[Any, CacheT, Name], freq: Duration, factor: Int)(handler: => U)
      (implicit impl: Lockable[CacheT, Name]): U = impl.withLock(id, freq, factor)(handler)

    def push[T, CacheT <: CacheType, Name <: CacheName, CacheEventT <: CacheEvent]
      (id: CacheId[T, CacheT, Name], e: T, evictionTime: Duration = 0.seconds)
      (implicit impl: Pushable[T, CacheT, Name]): T = impl.push(id, e, evictionTime)

    def genId[CacheT <: CacheType, Name <: CacheName](id: CacheId[Any, CacheT, Name])(implicit impl: Identifiable[CacheT, CacheName]) = impl.genEntryId(id)
  }

}


object QueryModel extends QueryModel

trait QueryModel extends PropsMacro {

  implicit class ToHazelPredicate[T](p: Pred[T]) {
    def asHazel[K, T]: Predicate[K, T] = (p match {
      case And(p1, p2) => Predicates.and(p1.asHazel, p2.asHazel)
      case Or(p1, p2) => Predicates.or(p1.asHazel, p2.asHazel)
      case Like(p1, p2) => Predicates.like(p1, p2)
      case Equal(p1, p2) => Predicates.equal(p1, p2)
    }).asInstanceOf[Predicate[K, T]]
  }


  sealed trait Pred[+T]
  case class Equal[+T](lprop: String, rprop: String) extends Pred[T]
  case class Like[+T](lprop: String, rprop: String) extends Pred[T]
  case class And[+T](lpred: Pred[T], rpred: Pred[T]) extends Pred[T]
  case class Or[+T](lpred: Pred[T], rpred: Pred[T]) extends Pred[T]
  case class all[+T]() extends Pred[T]


  implicit class RichPred[T](p: Pred[T]) {
    def && (p2: Pred[T]) = And[T](p, p2)
    def || (p2: Pred[T]) = Or[T](p, p2)
  }

  implicit class RichProp[T](s: Prop[T]) {
    def === (s2: String) = Equal[T](s.path, s2)
    def ~~~ (s2: String) = Like[T](s.path, s2)
  }

}
