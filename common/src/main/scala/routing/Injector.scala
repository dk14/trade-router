package routing

import com.hazelcast.core.HazelcastInstance
import routing.impl.Dispatcher

import scala.collection.concurrent.TrieMap

/**
 * Created by user on 11/8/15.
 */
trait Injector[D <: ToInject] {

  def inject[T](handler: D => T): T

}

object Injectors {
  private val injectors = new TrieMap[String, Injector[_ <: ToInject]]()

  def register[D <: ToInject](name: String, i: Injector[D]) = injectors.put(name, i)

  def apply[D <: ToInject](name: String): Injector[D] =
    injectors.get(name).getOrElse(sys.error(s"Injector $name is not registered!")).asInstanceOf[Injector[D]]


}

object injections {

  case class Common(deps: Deps) extends IO with ScriptRegistry
  val injector = Injectors[Deps]("default")
  val common = injector.inject(Common)

}

trait ToInject

case class Deps(@deprecated hazel: HazelcastInstance, multicache: Dispatcher) extends ToInject
