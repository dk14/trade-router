package routing

import java.util.UUID
import java.util.concurrent.TimeoutException

import com.hazelcast.core.{ EntryEvent, HazelcastInstance}
import com.hazelcast.map.listener.{EntryUpdatedListener, EntryAddedListener, MapListener}

import scala.collection.JavaConverters._
import scala.collection.concurrent.TrieMap
import scala.io.Source
import scala.reflect.runtime._
import scala.util.Try
import scalaz._, Scalaz._
import scalaz.concurrent.Task
import \/._


object RegistryHolder {

  import scala.reflect.runtime._
  import scala.tools.reflect.ToolBox

  val compiled = new TrieMap[String, Any]()
  var loader = getClass.getClassLoader

  val cm = universe.runtimeMirror(getClass.getClassLoader)
  val tb = cm.mkToolBox()

  type Stop = () => Unit

  def start(): Stop = {
    injections.injector.inject{ deps =>

      lazy val cache = deps.hazel.getMap[String, Script]("scripts")

      lazy val compilationResult = deps.hazel.getTopic[Response]("compiled")

      lazy val listener = new EntryAddedListener[String, Script] with EntryUpdatedListener[String, Script] {

        def process(entryEvent: EntryEvent[String, Script]): Unit = {
          val k = entryEvent.getKey
          val v = entryEvent.getValue
          val evaluate = Task {
            val catched = fromTryCatchThrowable[Option[Any], Throwable] {
              compiled.put(k, tb.eval(tb.parse(v.data)))
            }
            injections.injector.inject(RegistryImpl).msg(s"[run]:[$k]" + catched)
            catched.valueOr(throw _)
            "OK"
          }.attemptRunFor(5000).handleError {
            case t: TimeoutException => "RUNNING".right
            case x => x.printStackTrace(); x.left
          }.leftMap(_.getMessage).merge

          compilationResult.publish(Response(evaluate, v.correlationId))

        }

        override def entryAdded(entryEvent: EntryEvent[String, Script]): Unit =
          if(!entryEvent.getValue.isLocal) process(entryEvent)

        override def entryUpdated(entryEvent: EntryEvent[String, Script]): Unit = process(entryEvent)

      }

      val id = cache.addEntryListener(listener, true)

      () => cache.removeEntryListener(id)
    }
  }
}

case class Script(data: String, correlationId: String = UUID.randomUUID().toString, isLocal: Boolean = false)
case class Response(result: String, correlationId: String)

trait ScriptRegistry {
  import RegistryHolder._

  val deps: Deps
  lazy val cache = deps.hazel.getMap[String, Script]("scripts")
  private def map = cache.asScala

  def obtain[T](name: String) = {
    def externalScript = Source.fromInputStream(loader.getResourceAsStream(name.replaceAll(".scala", "") + ".scala")).mkString
    val data = map.getOrElseUpdate(name, Script(externalScript, isLocal = true))
    val catched = fromTryCatchThrowable[T, Throwable] {
      compiled.getOrElseUpdate(name, tb.eval(tb.parse(data.data))).asInstanceOf[T]
    }
    msg(s"[run]:[$name]" + catched)
    catched.valueOr(throw _)
  }


  import scalaz.concurrent._
  import scalaz.stream._

  def obtainProcess[In, Out](name: String) = (in: In) => obtain[In => Process[Task, Out]](name)(in)

  import coordination.ec
  def runAsyncScript(name: String, handler: Throwable => Unit = _.printStackTrace()) = Task.fork {
    Task {
      obtain[Any](name)
    }
  }.runAsync(_.leftMap(x => handler(x.getCause)))

  def msg(text: String) = {
    val id = deps.hazel.getIdGenerator("events").newId()
    deps.hazel.getMap[Long, Event[Message]]("events").put(id, Event(" [console]", IoMessage(text, "*.scala"), text, id))
    "OK"
  }

  def console(data: String) = {
    val id = deps.hazel.getIdGenerator("events").newId()
    deps.hazel.getMap[Long, Event[Message]]("events").put(id, Event(" [console]", IoMessage(data,  s"*.scala"),
      "\n" + ("-" * 100) + "\n" + data + "\n" + ("-" * 100) + "\n", id))
    "OK"
  }
}

case class RegistryImpl(deps: Deps) extends ScriptRegistry

