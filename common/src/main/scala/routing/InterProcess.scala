package routing

import java.util.UUID
import java.util.concurrent.{Executors, TimeUnit}
import routing.MultiCache._

import scala.collection.JavaConverters
import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scalaz._, Scalaz._
import scalaz.concurrent.Task
import MultiCache.dsl._


case class InterProcessImpl(deps: Deps) extends InterProcess //Case class is used as fluent constructor, not as ADT/GADT, should be rewritten

object coordination {
  implicit lazy val default = InterProcessImpl(injections.common.deps)
  implicit val ec = ExecutionContext.fromExecutorService(Executors.newCachedThreadPool())
}

trait InterProcess extends LeaderElection with Awaiter with Watcher with ProcessStates {

  val deps: Deps

  def ready[T](name: String)(status: Throwable \/ T): Throwable \/ T

  def await[T](name: String): Throwable \/ T

  def alive[T](name: String, status: Throwable \/ T, lease: Duration = 1 second): Throwable \/ T

  def living[T]: List[Ephemeral[T]]

  def mutex[T](name: String, freq: Duration = 3.seconds, factor: Int = 2)(handler: => T): T

}

trait Unsubscribe {
  def unsibscribe(): Unit
}

import InterProcessMessaging._

trait LeaderElection {

  val deps: Deps
  import deps.multicache._

  type WithLock = Lockable[PreferForLocks, DynamicName]

  def mutex[T](name: String, freq: Duration = 3.seconds, factor: Int = 2)(handler: => T): T = { //you should block to hold the lock
    withLock(NonTypedId[Any, PreferForLocks](name), freq, factor)(handler)
  }
}

trait Awaiter {
  sf: InterProcess =>

  val deps: Deps

  def await[T](name: String): Throwable \/ T = {
    println("waitFor " + name)
    receiveOnce(name).get.run.join
  }

  def ready[T](name: String)(status: Throwable \/ T): Throwable \/ T = { //TODO DSL
    publish(name, status)
    status
  }

  def on[T](name: String)(handle: Throwable \/ T => Unit): Unsubscribe = {
    val id = UUID.randomUUID().toString
    register(name, id, handle)
    new Unsubscribe {
      override def unsibscribe(): Unit = unregister(name, id)
    }
  }

  implicit class RichName(name: String) {
    def await[T]: Throwable \/ T = sf.await[T](name)
    def ![T](status: T): Throwable \/ T = sf.ready[T](name)(status.right)
    def :-|[T](status: Throwable): Throwable \/ T = sf.ready[T](name)(status.left)
    def :-|[T](status: Throwable \/ T): Throwable \/ T = sf.ready[T](name)(status)
  }

  implicit class RichDisjunction[T](d: Throwable \/ T){
    def ==> (name: String) = ready(name)(d)
    def alive(name: String) = sf.alive[T](name, d)
  }

}



trait Watcher extends LeaderElection with impl.Heartbeats {

  def hazel = deps.hazel

  case class Ephemeral[T](status: Throwable \/ T, lifeSigns: Boolean)

  val deps: Deps

  private def ephemeral(name: String, lease: Duration, factor: Int = 2): Unit = {
    val pinger = new Heartbeat(name, lease, factor)
    pinger.tryStart
  }

  def alive[T](name: String, status: Throwable \/ T, lease: Duration = 1 second): Throwable \/ T = {
    val statuses = deps.hazel.getMap[String, Throwable \/ T]("status")
    statuses.put(name, status)
    ephemeral(name, lease)
    status
  }

  import scalaz.stream.time._
  def watchdog[T](name: String, d: Duration = 1 second)(handler: List[Ephemeral[T]] => Unit): Unit = Task{
    mutex(name) {
      awakeEvery(d).map(_ => handler(living)).run.run
    }
  }.runAsync(_.leftMap(_.printStackTrace()))

  import JavaConverters._
  def living[T]: List[Ephemeral[T]] =
    deps.hazel.getMap[String, Throwable \/ T]("status").entrySet.asScala map { entry =>
      Ephemeral(entry.getValue, isAlive(entry.getKey))
    } toList

}

trait ProcessStates {
  trait ProcessState
  trait StartIsFine extends ProcessState
  trait StopIsFine extends ProcessState
  case object Init extends StartIsFine with ProcessState
  case object Stopped extends StartIsFine with ProcessState
  case object Interrupted extends StartIsFine with ProcessState
  case object Invalid extends StartIsFine with StopIsFine with ProcessState
  case object Started extends StopIsFine with ProcessState

  case object Killed extends RuntimeException

}

object Exit extends RuntimeException {
  def exit() = throw this

  def suicide() = throw this

  lazy val currentBundle = System.getProperty("bundle", "default")

  def checkBundleFor(component: String)(implicit interProc: InterProcess): Unit = {
    val bundle = interProc.await(component + ".bundle").leftMap(_ => "default").merge
    if (bundle != currentBundle) exit()
  }
}