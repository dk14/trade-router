package routing

import java.util.concurrent.TimeUnit
import scalaz._, Scalaz._
import com.hazelcast.core._
import scala.collection.concurrent.TrieMap
import scala.concurrent.duration._
import scalaz.concurrent.Task
import scalaz.stream._
import async._
import scala.reflect.runtime.universe._

import scalaz.stream.async.mutable._
import scalaz.stream.async._

import QueryModel._
import MultiCache._
import dsl._

trait IO extends CommonCacheRegistry {

  val deps: Deps
  import deps.multicache._

  type TradeEvent[T <: Trade] = Event[TradeMessage[T]]

  def snapshot[T <: Trade, CacheT <: CacheType, Name <: CacheName](
        name: String,
        to: CacheId[TradeEvent[T], CacheT, Name] = eventsCacheId[TradeMessage[T]],
        label: String = "[snapshot]",
        evictionTime: Duration = 0.seconds)(implicit pushable: Pushable[TradeEvent[T], CacheT, Name]): Sink[Task, Event[TradeMessage[T]]] =

    Process constant { e: TradeEvent[T] =>
      Task {
        val ne = e.copy(name = name, status = label)
        push(to, ne, evictionTime)
      }
    }


  def projection[T <: Trade, U, CacheT <: CacheType, Name <: CacheName](
        name: String,
        to: CacheId[U, CacheT, Name],
        evictionTime: Duration = 0.seconds,
        from: CacheId[TradeEvent[T], CacheT, Name] = eventsCacheId[TradeMessage[T]],
        withMaster: Boolean = true)(project: Event[TradeMessage[T]] => U)
          (implicit pushableU: Pushable[U, CacheT, Name], pushable: Pushable[TradeEvent[T], CacheT, Name]): Sink[Task, Event[TradeMessage[T]]] =

    Process constant { e: TradeEvent[T] =>
      Task {
        if (withMaster) pushable.push(from, e.copy(name = name, status = "[snapshot]"), evictionTime)
        pushableU.push(to, project(e), evictionTime)
      }
    }

  type Handler[T, U] = Process[Task, Event[IoMessage[T]]] => Process[Task, U]
  type Q[T] = Queue[Event[IoMessage[T]]]

  case class Worker[T, U](q: Q[T], receive: Process[Task, U])

  import Process._

  trait EndpointLifecycle[T] {
    trait Command
    case class ProcessMessage(e: E[T]) extends Command
    case class Clean(cause: Cause = Cause.End) extends Command

    val controlTopic = topic[Command]() //to receive commands from components/*.scala

    var inputTopic: Option[Topic[CacheNotification[E[T]]]] = none

    def start(): Unit

    def stop(): Unit

  }

  type E[T] = Event[IoMessage[T]]

  import org.apache.commons.lang3.exception.ExceptionUtils._

  def reportAndContinue[T](e: Option[E[T]] = none)(cause: Throwable) = { //TODO save all failed events (from cause's trace) to cache
    val trace = getRootCauseStackTrace(cause).mkString("\n")
    val ne = e.getOrElse(Event("[uncknown]", IoMessage(cause.getStackTrace.toString)))
    push(eventsCacheId[IoMessage[T]], ne.copy(name = cause.getMessage, status = "[failed]"))
    println(trace)
    halt //normal termination; may not be correct as new versions of trade should not be processed
  }

  def endpoint[T, U, CacheT <: CacheType, Name <: CacheName](
        id: CacheId[E[T], CacheT, Name])
        (handler: => Handler[T, U])
        (implicit subscribable: Subscribable[E[T], CacheT, Name, EventNew]): EndpointLifecycle[T] =
    new EndpointLifecycle[T] {

      def save(e: E[T]): Unit = {
        println("Received:" + e.correlationId + " <=== " + id.name)
        push(eventsCacheId[IoMessage[T]], e.copy(msg = e.msg.copy(isIncoming = true, endpoint = id.name), status = "[received]"))
      }

      override def start(): Unit = {

        require(inputTopic.isEmpty, s"${id.name} is already started!")

        inputTopic = subscribable.subscribe(id, all[E[T]], save).some //subscribe on notification
        val processing = inputTopic.get.subscribe //subscribe on topic with notifications
          .map(_.value)
          .map(ProcessMessage)
          .onHalt(c => emit(Clean(c)))

        val supervisor = (processing merge controlTopic.subscribe)
          .scan(Map.empty[String, Topic[E[T]]]) {
            case (handlers, ProcessMessage(e)) =>
              val worker = handlers.getOrElse(e.concurrentId, {
                val worker = topic[E[T]]()
                handler(worker.subscribe)
                  .onFailure(reportAndContinue(e.some))
                  .run
                  .runAsync(_.leftMap(_.printStackTrace()))
                worker
              })
              worker.publishOne(e).run
              handlers ++ Map(e.concurrentId -> worker)
            case (handlers, Clean(c)) =>
              handlers.values.foreach(_.close.run)
              Map.empty
          }
        supervisor.onFailure(reportAndContinue()).run.runAsync(_.leftMap(_.printStackTrace()))
      }

      override def stop(): Unit = {
        inputTopic.get.close.run
        inputTopic = none
      }
  }

  def sink[T, CacheT <: CacheType, Name <: CacheName](dest: CacheId[E[T], CacheT, Name], flowId: String = "")
                                                     (implicit i: Pushable[E[T], CacheT, Name]): Sink[Task, E[T]] =
    Process constant { e: Event[IoMessage[T]] =>
      Task {
        val q = prop[e.type, String](_.correlationId) === e.correlationId && prop[e.type, String](_.status) === "[sent]"
        def predicate(c: Event[IoMessage[T]]) = c.msg.endpoint == dest.name && c.msg.flowId == flowId && c.name == e.name

        val cache = implicitly[QueryableWithoutKey[E[T], PreferForSnapsots, events]]//TODO add to dsl

        if (!cache.query(eventsCacheId[IoMessage[T]], q).exists(predicate)) {
          push(dest, e)
          push(eventsCacheId[IoMessage[T]], e.copy(status = "[sent]", msg = e.msg.copy(endpoint = dest.name, flowId = flowId, isIncoming = false)))
        } else {
          push(eventsCacheId[IoMessage[T]], e.copy(status = "[duplicate]", msg = e.msg.copy(endpoint = dest.name, flowId = flowId, isIncoming = false)))
        }
        println("SendTo:" + dest.name + " <=== " + e.correlationId)
      }
    }

}

case class IOImpl(deps: Deps) extends IO

