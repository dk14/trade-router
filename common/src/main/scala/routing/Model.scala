package routing
import scalaz._, Scalaz._

case class Event[+T <: Message](correlationId: String,
                                msg: T, name: String = "message",
                                id: Long = 0,
                                concurrentId: String = "allInOne",
                                version: Int = -1,
                                status: String = "completed")

sealed trait Message

case class MatchingHeaders(matchingId: String)

case class IoMessage[+T](raw: T, endpoint: String = "[unknown-source]", isIncoming: Boolean = true, headers: Map[String, String] = Map.empty[String, String], flowId: String = "") extends Message

case class TradeMessage[+T <: Trade](messageId: String, trade: T) extends Message

sealed trait Trade

trait WithEvent[+E] extends Trade {val event: E}

case class ExecutionAdvice[+E <: TradeEvent](header: TradeHeader, event: E) extends Trade with WithEvent[E]

case class TradeHeader(ids: List[TradeId], allocationId: Option[String] = none)

case class TradeId(party: Party, id: String)

case class Party(id: String, name: String)

trait TradeEvent

case class New(notional1: Int = 5, notional2: Int = 6, notional3: Int = 7, notional: Option[Int] = none) extends TradeEvent

case class Amend(linkId: String) extends TradeEvent


