package routing

import com.hazelcast.core
import com.hazelcast.core.MessageListener

import scalaz.\/
import scalaz.concurrent.Task
import scalaz.stream._
import MultiCache._


object Helpers {
  import scala.reflect._

  type ExecNew = Event[TradeMessage[ExecutionAdvice[New]]]
  type ExecAmend = Event[TradeMessage[ExecutionAdvice[Amend]]]
  type ExecBase = Event[TradeMessage[ExecutionAdvice[TradeEvent]]]

  type TradeNew = ExecutionAdvice[New]
  type TradeAmend = ExecutionAdvice[Amend]
  type TradeBase = ExecutionAdvice[TradeEvent]

  type InputEvent[T] = Event[IoMessage[T]]
  type OutputEvent[T] = Event[IoMessage[T]]

  type P[T] = Process[scalaz.concurrent.Task, T]

  import scalaz._, Scalaz._

  case class typeCheck[Tr <: Trade: ClassTag, TE <: TradeEvent: ClassTag](implicit ev: Tr <:< WithEvent[TE]) { //not case class really

    def isOfType[T <: TradeMessage[_]](e: Event[T]): Boolean =
      classTag[TE].runtimeClass.isInstance(e.msg.trade.asInstanceOf[WithEvent[TE]].event) &&
        classTag[Tr].runtimeClass.isInstance(e.msg.trade)

    def toOpt[T <: TradeMessage[_]](e: Event[T]): Option[Event[TradeMessage[Tr]]] =
      if (isOfType(e)) e.asInstanceOf[Event[TradeMessage[Tr]]].some else none

    import Process._
    def toProcess[T <: TradeMessage[_]](e: Event[T]): P[Event[TradeMessage[Tr]]] =
      if (isOfType(e)) emit(e.asInstanceOf[Event[TradeMessage[Tr]]]) else halt
  }

  def agent[S, In](zero: S)(f: (S, In) => S) = new AsyncAgent[In, S](zero)(f)

  @deprecated("Will be removed after migration of InterProcessMessaging to MultiCache API")
  class AsyncAgent[In, S](zero: S)(f: (S, In) => S) { //guarantees atomic operation, like Akka Agents, but not Akka :)

    import scalaz.stream.async._
    import scalaz.concurrent._
    import scalaz._

    private case class Effect[In, State](i: In, apply: (Throwable \/ S) => Unit)

    private def applyWithEffect(s: S, i: Effect[In, S]) = {
      val r = \/.fromTryCatchNonFatal(f(s, i.i))
      i.apply(r)
      r | zero
    }

    private val push = unboundedQueue[Effect[In, S]]

    def send(i: In) = Task.async[S](handler => push.enqueueOne(Effect(i, handler)).run)

    def close() = push.close

    def fail(t: Throwable) = push.fail(t)

    val process = push.dequeue.scan(zero)(applyWithEffect)

  }

}

import Helpers._

trait Transformer[I, O] extends (I => O) with FailureHandling {
  def transform: I => O
  override def apply(t: I): O = transform(t)
}

trait TransformWithProcess[I, O] extends Transformer[I, P[O]] {

  def transformId = "transformation"

  def transformWithProcess: I => O

  def transform: I => P[O] = i => Process.eval(Task.delay(transformWithProcess(i))) onFailure specifyWithInput(transformId, i)

}

trait TransformWithTask[I, O] extends Transformer[I, P[O]] {

  def transformWithTask: I => Task[O]

  def transform: I => P[O] = i => Process.eval(transformWithTask(i))

}

import scalaz.stream._

case class ExceptionWithContext[T](context: String, e: Option[T], t: Throwable) extends Throwable {
  override def getMessage() = s"[$context]" + super.getMessage()
  override def getCause() = t
}

trait FailureHandling extends CommonCacheRegistry { //TODO simplify that?
  import scalaz._, Scalaz._

  val deps: Deps
  import deps.multicache._

  def preprocessEvents[T](pe: T): T = pe match { //accurate ordering
    case e: Event[_] =>
      val id = implicitly[Identifiable[PreferForSnapsots, events]].genEntryId(eventsCacheId[Message])
      e.copy(id = deps.hazel.getIdGenerator("events").newId()).asInstanceOf[T]
    case x => x
  }

  import Process._
  def specify[T](context: String)(t: Throwable): P[T] = fail(ExceptionWithContext(context, none, t))
  def specifyWithInput[T, U](context: String, i: T)(t: Throwable): P[U] = fail(ExceptionWithContext(context, i.some, t))

  implicit class ErrorContextInfo[T](input: P[T]){
    def failsafe[U](name: String)(flow: P[T] => P[U]) = input flatMap { i =>
      val ei = preprocessEvents(i)
      flow(emit(i)) onFailure specifyWithInput[T, U](name, ei)
    }
  }

}

trait Flow[T, U] extends (Process[Task, T] => Process[Task, U]) with FailureHandling {

  def flowId: String
  import Process._

  def apply(input: Process[Task, T]) = input.failsafe(flowId)(flow)
  def flow: Process[Task, T] => Process[Task, U]

}



import com.softwaremill.quicklens._
/**
 * Uses one topic for all inter-process communication. Newbie process sees only last message from subsciption
 *
 * It's written to use one topic for all communication. Should be migrated to MultiCache and simplified
 */
object InterProcessMessaging { //TODO simplify that?

  import scalaz._, Scalaz._
  import injections.common._

  case class Handler(callback: Throwable \/ ProcessMessage => Unit, id: String, isPermanent: Boolean)

  type MultiMapHandler = Map[String, List[Handler]]
  type LastMessagePerNotification = Map[String, ProcessMessage]

  case class NotificationMeta(handlers: Set[Handler] = Set.empty, lastMessage: Option[ProcessMessage] = none,
                              ignoreList: Set[String] = Set.empty)

  case class State(m: Map[String, NotificationMeta] = Map.empty)

  trait Command
  case class Register(name: String, handler: Handler) extends Command
  case class ProcessMessage(name: String, body: Any) extends Command
  case class Unregister(name: String, id: String) extends Command

  val messenger = agent[State, Command](State()) { //TODO simplify
    case (s, Register(name, h))  =>
      val meta = s.m.getOrElse(name, NotificationMeta())

      val newmeta = if (!meta.ignoreList.contains(h.id) && meta.lastMessage.nonEmpty){
        h.callback(meta.lastMessage.get.right)
        meta.modify(_.ignoreList).using(_ + h.id)
      } else meta.modify(_.handlers).using(_ + h)

      s.modify(_.m).using(_ ++ Map(name -> newmeta))
    case (s, m: ProcessMessage) =>
      val meta = s.m.getOrElse(m.name, NotificationMeta())

      meta.handlers.foreach(_.callback(m.right))
      val newmeta = meta.copy(handlers = meta.handlers.filterNot(_.isPermanent), lastMessage = m.some)

      s.modify(_.m).using(_ ++ Map(m.name -> newmeta))
    case (s, m@Unregister(name, id)) =>
      val meta = s.m(name)

      val newmeta = meta.modify(_.handlers).using(_.filterNot(_.id == id))
      s.modify(_.m).using(_ ++ Map(m.name -> newmeta))
  }

  def start(): Unsubscribe = {
    val topic = deps.hazel.getTopic[ProcessMessage]("inter-process")
    messenger.process.run.runAsync(_.leftMap(_.printStackTrace()))

    val listener = new MessageListener[ProcessMessage] {
      override def onMessage(message: core.Message[ProcessMessage]): Unit = {
        messenger.send(message.getMessageObject).run
      }
    }

    val id = topic.addMessageListener(listener)

    new Unsubscribe {
      override def unsibscribe(): Unit = topic.removeMessageListener(id)
    }
  }

  def receiveOnce[T](name: String) = {
    val task = Task.async[ProcessMessage]{ handle =>
      messenger.send(Register(name, Handler(handle, Thread.currentThread().getName, false))).run
    }
    task.map(_.body.asInstanceOf[Throwable \/ T])
  }

  def register[T](name: String, id: String, handle: Throwable \/ T => Unit) = {
    val register = Register(name, Handler(_.rightMap(_.body.asInstanceOf[T]) |> handle, id, true))
    messenger.send(register).run
  }

  def unregister(name: String, id: String) = messenger.send(Unregister(name, id)).run

  def publish[T](name: String, body: T) = {
    deps.hazel.getTopic[ProcessMessage]("inter-process").publish(ProcessMessage(name, body))
  }
}



