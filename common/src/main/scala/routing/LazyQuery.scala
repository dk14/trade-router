package routing

import scalaz._, Scalaz._
import Helpers._

trait LazyQuery extends IO { //TODO restore suspend/resume notifications to the event cache; remove matched events (lost after refactoring)

  val deps: Deps

  import MultiCache._
  import QueryModel._
  import dsl._

  import scala.reflect.runtime.universe._

  def matching[T, U, CacheT <: CacheType, Name <: CacheName]
    (matchingCache: CacheId[T, CacheT, Name], p: Pred[T])
    (check: P[Set[T]] => P[U])
    (implicit impl: SubscribableOnce[T, CacheT, Name, RepeatableQueryResponse], impl2: Queryable[_, T, CacheT, Name]): P[U] = {

      val source = subscribeOnce(matchingCache, p)(_.collect {
        case CacheNotification(not: RepeatableQueryResponse, _, _, seq) => seq.toSet //TODO move CacheEvent to CacheNotification's polymorphic type to avoid pattern matching
      })
      check(source)
  }
}