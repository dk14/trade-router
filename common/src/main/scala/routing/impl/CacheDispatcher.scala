package routing.impl


import java.util.concurrent.{TimeUnit, TimeoutException}
import java.util.concurrent.atomic.AtomicBoolean

import com.hazelcast.core._
import com.hazelcast.map.listener.{MapListener, EntryRemovedListener, EntryAddedListener}
import com.hazelcast.query.{Predicates, Predicate}
import routing.MultiCache._
import routing._
import routing.QueryModel._
import scala.collection.JavaConverters._
import scala.concurrent.duration.Duration
import scalaz.concurrent.Task
import scalaz._
import scalaz.stream._
import async._
import \/._
import scalaz.stream.async.mutable.{Topic, Queue}
import Helpers._

trait Dispatcher extends HazelCastCacheImpl{
  type PreferForLocks = HazelCastCache
  type PreferForMatching = HazelCastCache
  type PreferForSnapsots = HazelCastCache
  type Io = HazelCastTopic
}

object DefaultDispatcher extends Dispatcher {
  override lazy val hazel: HazelcastInstance = Hazelcast.newHazelcastInstance()
}

trait HazelCastCache extends CacheType
trait HazelCastTopic extends CacheType
trait HazelCastQueue extends CacheType

trait HazelCastCacheImpl extends Heartbeats { //TODO split to separate traitsand files

  def hazel: HazelcastInstance

  implicit val sc2 = new java.util.concurrent.ScheduledThreadPoolExecutor(1)

  implicit def cacheBasic[K, V, Name <: CacheName, T <: (K,V)] = new AnyRef
    with Queryable[K, V, HazelCastCache, Name]
    with Readable[K,V, HazelCastCache, Name]
    with RemovableByKey[K, V, HazelCastCache, Name]
    with Updatable[K, V, HazelCastCache, Name] {

    override def query(id: Id, p: Pred[V]): Seq[EntryT] =
      hazel.getMap[K,V](id.name).entrySet(p.asHazel).asScala.map(x => x.getKey -> x.getValue)toSeq

    override def read(id: Id, k: K): V = hazel.getMap[K,V](id.name).get(k)

    override def removeByKey(id: Id, k: K): \/[Throwable, EntryT] = fromTryCatchThrowable[EntryT, Throwable](k -> hazel.getMap[K,V](id.name).remove(k))

    override def update(id: Id, k: K, v: V): Option[V] = Option(hazel.getMap[K,V](id.name).put(k, v))

  }

  implicit def identityGenHazel[Name <: CacheName]= new Identifiable[HazelCastCache, Name]{
    override def genEntryId(id: Id): Long = hazel.getIdGenerator(id.name).newId()
  }

  implicit def queryWithoutKey[V, CacheT <: CacheType, Name <: CacheName] = new QueryableWithoutKey[V, CacheT, Name]{}

  implicit def pushToCache[V, Name <: CacheName] = new Pushable[V, HazelCastCache, Name] {
    override def push(id: Id, e: V, evictionTime: Duration): V = {
      val newId = hazel.getIdGenerator(id.name).newId()
      hazel.getMap[Long,V](id.name).put(newId, e, evictionTime.toMillis, TimeUnit.MILLISECONDS)
      e
    }
  }


  implicit def pushToSomeEventCache[V <: routing.Message, Name <: CacheName] = new Pushable[Event[V], HazelCastCache, Name] {
    override def push(id: Id, e: Event[V], evictionTime: Duration): Event[V] = {
      val newId = hazel.getIdGenerator(id.name).newId()
      val newE = e.copy(id = newId)
      hazel.getMap[Long,Event[V]](id.name).put(newId, newE, evictionTime.toMillis, TimeUnit.MILLISECONDS)
      newE
    }
  }

  implicit def topicBasic[V, Name <: CacheName] = new Pushable[V, HazelCastTopic, Name]{
    override def push(id: Id, e: V, evictionTime: Duration): V = {
      hazel.getTopic[V](id.name).publish(e)
      e
    }
  }

  //just example of implicit dispatching
  implicit def topicBasicString[Name <: CacheName] = new Pushable[String, HazelCastTopic, Name]{
    override def push(id: Id, e: String, evictionTime: Duration): String = {println(e); e}
  }

  implicit def topicSubscribers[V, Name <: CacheName] = new Subscribable[V, HazelCastTopic, Name, EventNew] {

    override def subscribe(id: Id, query: Pred[V], handle: V => Unit = _ => Unit): Topic[CacheNotification[V]] = {//TODO Predicate
      val producer = topic[CacheNotification[V]]()

      val listener: MessageListener[V] = new MessageListener[V]{
        override def onMessage(message: com.hazelcast.core.Message[V]): Unit = {
          handle(message.getMessageObject)
          producer.publishOne(CacheNotification(new EventNew{}, message.getMessageObject)).run
        }
      }

      val subscriptionId: String = hazel.getTopic[V](id.name).addMessageListener(listener)

      producer.subscribe.onHalt{ _ => //when topic is closed from outside
        hazel.getTopic[V](id.name).removeMessageListener(subscriptionId)
        Process.halt
      }

      producer
    }
  }

  import scala.concurrent.duration._
  case class SubscribtionPolling(d: Duration)

  implicit def cacheSubscribersQ[V, Name <: CacheName, CacheT <: CacheType] = new SubscribableOnce[V, CacheT, Name, RepeatableQueryResponse] {

    val conf: SubscribtionPolling = SubscribtionPolling(1 second)

    override def subscribeOnce[U](id: Id, query: Pred[V])(handler: P[CacheNotification[V]] => P[U])(implicit q: Queryable[_, V, CacheT, Name]): P[U] = {
      import scalaz.stream.time._
      import Process._, Task._

      val qid = id.asInstanceOf[q.Id] //it's just copy
      val listener = (eval(delay(0 seconds)) ++ awakeEvery(conf.d)) //quick first notification
        .map(_ => q.query(qid, query))
        .map(_.map(_._2))
        .map(seq => CacheNotification(new RepeatableQueryResponse{}, seq.head, None, seq.toList))
      handler(listener).take(1)
    }
  }



  @deprecated("not using this impl; requires some guarantees from hazel, like it should return from addEntryListener only when listener became really active.")
  implicit def cacheSubscribers[V, Name <: CacheName] =
    new SubscribableOnce[V, HazelCastCache, Name, EventNew with EventRemoved with QueryResponse] {


    override def subscribeOnce[U](id: Id, query: Pred[V])(handler: P[CacheNotification[V]] => P[U])(implicit q: Queryable[_, V, CacheT, Name]): P[U] = {
      val agent = topic[CacheNotification[V]]()
      val listener = new EntryAddedListener[Any, V] with EntryRemovedListener[Any, V]{
        override def entryAdded(e: EntryEvent[Any, V]): Unit =
          agent.publishOne(CacheNotification(new EventNew {},  e.getValue))

        override def entryRemoved(e: EntryEvent[Any, V]): Unit =
          agent.publishOne(CacheNotification(new EventRemoved {},  e.getValue))
      }
      val subId = hazel.getMap[Any,V](id.name).addEntryListener(listener, query.asHazel[Any, V], true)

      val queried = hazel.getMap[Any,V](id.name).entrySet(query.asHazel[Any, V])
        .asScala.map(x => CacheNotification(new QueryResponse{}, x.getValue))
      val prepend = queried.toSeq.map(Task.delay(_)).map(Process.eval).reduce(_ ++ _)

      handler(prepend ++ agent.subscribe).once.map{ x =>
        agent.close.runAsync(_.leftMap(_.printStackTrace()))
        hazel.getMap[Any,V](id.name).removeEntryListener(subId)
        x
      }
    }

  }


  implicit def lockable[Name <: CacheName] = new Lockable[HazelCastCache, Name] {
    override def withLock[U](id: Id, freq: Duration, factor: Int)(handler: => U): U = {
      val lock = hazel.getLock(id.name)
      val ping = new Heartbeat(id.name + ".mutex", freq, factor)
      ping.checkAlive(() => lock.forceUnlock()) //force unlock for died process

      lock.lockInterruptibly()
      try {
        ping.tryStart
        handler
      } finally {
        lock.unlock()
        ping.cancel
      }
    }
  }

  //Just examples

  val iddddd = cacheId[(String, String), HazelCastCache, CacheName]

  implicitly[Queryable[String, String, HazelCastCache, CacheName]].query(iddddd, prop[String, String](_.toString) === "5")

  val id2 = cacheId[String, HazelCastTopic, CacheName]

  implicitly[Pushable[String, HazelCastTopic, CacheName]].push(id2, "", null)


}

case class Time(v: Long)

trait Heartbeats {
  import scalaz.stream.time._
  import scalaz.stream.Process

  def hazel: HazelcastInstance

  implicit val sc = new java.util.concurrent.ScheduledThreadPoolExecutor(5)

  private def time = System.currentTimeMillis()

  def isAlive(name: String) = !hazel.getAtomicReference[Long](name + ".ping").isNull

  def downtime(name: String) = time - hazel.getAtomicReference[Time](name + ".ping").get().v

  class Heartbeat(name: String, every: Duration, factor: Int) {

    private val timers = hazel.getMap[String, Time]("ping")
    private val atomicCancel = new AtomicBoolean()

    def cancel = {
      timers.remove(name)
      atomicCancel.set(true)
    }

    def checkAlive(onTimeout: () => Unit) =
      awakeEvery(every * factor).map{_ =>
        if (isTimeout) {
          onTimeout()
          throw new TimeoutException("Heartebeat timeout for " + name)
        }
      }.run.runAsyncInterruptibly(_.leftMap(_.printStackTrace()), atomicCancel)

    def tryStart = if(!isRegistered || isTimeout) {
      awakeEvery(every).map(_ => timers.put(name, Time(time))).run.runAsyncInterruptibly(_.leftMap(_.printStackTrace()), atomicCancel)
    }

    def isTimeout = Option(timers.get(name)).map(mark => time - mark.v > every.toMillis * factor).getOrElse(false)
    private def isRegistered = !timers.containsKey(name)
  }
}