
package routing

import org.apache.xerces.impl.dv.xs.XSSimpleTypeDecl
import org.apache.xerces.impl.xs._
import org.apache.xerces.xs._

import scala.collection._
import scalaz.Scalaz._

case class NsRegistry(prefixToUri: Map[String, String] = Map()){
  lazy val uriToPrefix = prefixToUri.map{case (k, v) => (v, k)}.toMap
}

class ModelGeneratorImpl(val model: XSModel) extends ModelGenerator

trait ModelGenerator extends XsdNavigation {

  def model: XSModel
  val nsRegistry: NsRegistry = NsRegistry()

  private def makeMultiMap[K, V](l: Traversable[(K, V)]): Map[K, Set[V]] = l.groupBy(e => e._1).mapValues(e => e.map(x => x._2).toSet)


  val allTypes = model.getComponents(XSConstants.TYPE_DEFINITION).collect{
    case ed: XSComplexTypeDecl => ed
  }

  val allSubstitutions: Map[XSElementDecl, Set[XSElementDecl]] = makeMultiMap {
    model.getComponents(XSConstants.ELEMENT_DECLARATION) collect {
      case ed: XSElementDecl if ed.getSubstitutionGroupAffiliation != null =>
        (ed.getSubstitutionGroupAffiliation.asInstanceOf[XSElementDecl], ed)
    }
  }

  def getMeta(root: String) = model
    .getComponents(XSConstants.ELEMENT_DECLARATION)
    .map(_.asInstanceOf[XSElementDeclaration])
    .filter(_.getName == root).headOption
    .map(ed => processTerm(ed.getTypeDefinition)._1
      .flatMap(_.stringOfCaseClass).toSet)
    .head

  private def processSubstitutions(from: XSElementDecl) = List(from) ++ allSubstitutions.getOrElse(from, Set()).toList

  type Processed = Set[String]

  protected def processTerm(td: XSTypeDefinition, processed: Processed = Set.empty): (List[Member], Processed) = td match {
    case x: XSSimpleTypeDecl => nil[Member] -> processed
    case x: XSComplexTypeDecl =>

      if(x.getName == "Swap") println(x)
      getSubTermsOfType(x).foldLeft(nil[Member] -> processed) { case ((acc, processed), member) =>
      //println("generating..." + !processed.contains(member.typ) + "    " + member)
      val (membSub, newp) = if (!processed.contains(member.typ) &&  member.el.nonEmpty){
        val (subb, newp) = processTerm(member.el.get.getTypeDefinition, processed + member.typ)
        (member.copy(sub = distinct(subb)) :: subb) -> newp
      } else Nil -> processed
      (membSub ::: acc) -> newp
    }
  }
}

trait XsdNavigation {

  implicit val allTypes: Traversable[XSComplexTypeDecl]
  implicit val allSubstitutions: Map[XSElementDecl, Set[XSElementDecl]]
  implicit val nsRegistry: NsRegistry


  trait Generate
  case object Simple extends Generate
  case object Opt extends Generate
  case object GenList extends Generate


  lazy val Enums = ".*Enum".r

  implicit class RichStringProtection(s:String){
    def post = s match {
      case "this" => "`this`"
      case "type" => "`type`"
      case "date" => "String"
      case "decimal" => "String"
      case "NonNegativeDecimal" => "String"
      case "nonNegativeInteger" => "String"
      case "PositiveInteger" => "String"
      case "PositiveDecimal" => "String"
      case  Enums() => "String"
      case x => x
    }
  }

  def distinct(list: List[Member]) = {
    val names = list.groupBy(_.name).mapValues(_.size)
    list map { m =>
      if(names(m.name) > 1) m.copy(name = (m.name + m.typ).filter('`'!=), originalName = m.name.some) else m
    }
  }

  def getSubTermsOfType(from: XSComplexTypeDecl): List[Member] =
    processAbstractTypes(from) flatMap { t =>
      Option(t.getParticle).map(_.getTerm) match {
        case Some(g: XSModelGroupImpl) => recursiveGetSubTermsOfModelGroup(g)
        case Some(other) => termToMember(other, Simple).toList
        case _ => List.empty
      }
    }



  case class Member(name: String, typ: String, directive: Generate, el: Option[XSElementDecl] = None, sub: List[Member] = nil, originalName: Option[String] = none){
    def stringOfMember = directive match {
      case Simple => s"$name: $typ"
      case Opt => s"$name: Option[$typ]"
      case GenList => s"$name: List[$typ]"
    }

    private def params = ""//"\n    " + sub.map(_.stringOfMember).mkString(",\n    ")

    def stringOfCaseClass = el map {_ =>
      if(sub.isEmpty) s"type $typ = String" else s"case class $typ($params)"
    }
  }



  def termToMember(x: XSTerm, directive: Generate) = x match {
    case x: XSElementDecl => Member(x.qname.name.post, x.getTypeDefinition.getName.capitalize.post, directive, x.some).some
    case a: XSAttributeDecl => Member(x.qname.name.post, "String", directive).some
    case _ => none
  }


  def recursiveGetSubTermsOfModelGroup(g: XSModelGroupImpl, directive: Generate = Simple): List[Member] =  mergeModelGroups {
    val newDirective = (directive, g.getCompositor, g.minEffectiveTotalRange() > 0) match {
      case (_, _, true) => GenList
      case (GenList, _, _) => GenList
      case (_, XSModelGroupImpl.MODELGROUP_CHOICE, _) => Opt
      case (Opt, _, _) => Opt
      case (x, _, _) => x
    }

    g.getParticles map {
      case d: XSParticleDecl => d.getTerm match {
        case mg: XSModelGroupImpl => recursiveGetSubTermsOfModelGroup(mg, newDirective)
        case other => termToMember(other, directive).toList
      }
    } toList
  }

  private def processAbstractTypes(from: XSComplexTypeDecl): List[XSComplexTypeDecl] = {
    val possibleImplementations = allTypes.filter(_.getBaseType == from)
    List(from) ++ possibleImplementations
  }

  ///-----------------Helpers---------------------------------------------------
  private def mergeModelGroups(l: List[List[Member]]): List[Member] = {
    val equals: (Member, Member) => Boolean = {
      case (a: Member, b: Member) => a.name == b.name
      case _ => false
    }
    l.reduce(mergeListsOf[Member](_, _, equals))
  }

  private def mergeListsOf[T](acc: List[T], next: List[T], equals :(T, T) => Boolean = (_:T) == (_:T)) = {
    def insertWithCorrectPosition(to: List[T], e: T, order: List[T]) = {
      val afterElement = order.reverse.takeWhile(!equals(e, _)).reverse
      val firstAfterElement =  afterElement.map(x => to.indexWhere((y: T) => x.toString == y.toString))
      firstAfterElement.find(_ != -1) map {index =>
        to.take(index) ++ e.point[List] ++ to.drop(index)
      } getOrElse (to ++ e.point[List])
    }

    next.foldLeft(acc)(insertWithCorrectPosition(_, _, next)).distinct
  }

  private def TryList[T](f: => List[T]) = scala.util.Try(f).toOption getOrElse List.empty// TODO error handling

  implicit class RichObjectList(l: { def item(index: Int): XSObject; def getLength(): Int}) extends Traversable[XSObject] {
    def foreach[U](f: XSObject => U) {
      for(i <- 0 to (l.getLength - 1)) {
        f(l.item(i))
      }
    }
  }

  implicit class RichXsObject(o: XSObject)(implicit val nsRegistry: NsRegistry = NsRegistry()) {
    def qname = QName(o.getName, nsRegistry.uriToPrefix.get(o.getNamespace), Some(o.getNamespace))
  }

  val qnameRegexp = """([^:]*):([^:]*)""".r
  def strToOpt(s: String) = if(s.nonEmpty) Some(s) else None

  implicit class RichString(s: String)(implicit val nsRegistry: NsRegistry = NsRegistry()) {
    def qname = s match {
      case qnameRegexp(prefix, name) => QName(name, strToOpt(prefix), strToOpt(prefix).flatMap(nsRegistry.prefixToUri.get))
      case name => QName(name, None, None)
    }
  }
  case class QName(name: String, prefix: Option[String] = None, namespace: Option[String] = None){
    override def equals(any: Any) = any match {
      case t: QName => name == t.name && prefix ==== t.prefix && namespace ==== t.namespace
      case x => super.equals(x)
    }
    override def toString = prefix.map(_ + ":").getOrElse("") + name
  }

  implicit class RichOption[A](opt1: Option[A]) {
    def ====[B](opt2: Option[B]) = (opt1 |@| opt2) {_ == _} getOrElse false
  }

}