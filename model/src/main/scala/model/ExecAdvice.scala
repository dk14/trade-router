package model

 object execAdvice{
   case class FxTarget()

   case class Calculation()

   type ExerciseEvent = String

   type BusinessDateRange = String

   case class Cashflows()

   case class OptionFeatures()

   case class CancelableProvisionAdjustedDates()

   case class CashSettlementPaymentDate()

   case class SignedInfoType()

   case class IssuerTradeId()

   type PremiumQuote = String

   case class FxOutstandingGain()

   type PartyRelationshipType = String

   type FxAccrualLeverage = String

   type FxSettlementPeriodBarrier = String

   case class SignatureType()

   type NotionalReference = String

   case class CommodityPricingDates()

   type NotDomesticCurrency = String

   type FormulaComponent = String

   type DividendPeriodPayment = String

   type FxPivot = String

   case class Allocation()

   type ClassifiablePayment = String

   case class FxWeightedFixingSchedule()

   case class Address()

   type MandatoryEarlyTerminationAdjustedDates = String

   case class TransformsType()

   case class FailureToPay()

   type FeaturePayment = String

   case class ExtraordinaryEvents()

   case class SwapAdditionalTerms()

   type CalculationPeriodsScheduleReference = String

   type OptionStrike = String

   type CommoditySettlementPeriodsNotionalQuantitySchedule = String

   type SettlementPriceDefaultElection = String

   case class InformationSource()

   type SettlementTermsReference = String

   type RSAKeyValueType = String

   type AverageDailyTradingVolumeLimit = String

   type CommodityPhysicalEuropeanExercise = String

   type NonEmptyURI = String

   type FxFlexibleForwardRate = String

   case class OptionExercise()

   type MasterConfirmationAnnexType = String

   type FxKnockoutLevel = String

   type Region = String

   type DateTime = String

   case class FxTargetSettlementPeriod()

   case class RelativeDateSequence()

   type FxCrossRateObservable = String

   case class CompoundingRate()

   case class DSAKeyValueType()

   case class ContractualMatrix()

   type BusinessCentersReference = String

   type GenericProductFeature = String

   case class CalculationPeriod()

   type OffsetPrevailingTime = String

   type BrokerConfirmationType = String

   case class BrokerConfirmation()

   type CommodityBusinessCalendar = String

   type CrossCurrencyMethod = String

   type CommodityDetails = String

   case class FxTargetAccumulationRegion()

   type ActualPrice = String

   type PartyRole = String

   case class SettledEntityMatrix()

   type FxCounterCurrencyAmount = String

   case class FxTargetConstantPayoffRegion()

   case class FxSettlementRateSource()

   type CollateralType = String

   type PubliclyAvailableInformation = String

   type CommodityForwardLeg = String

   case class DividendAdjustment()

   case class Composite()

   case class ExtendibleProvision()

   type CommodityFxType = String

   type QuantityUnit = String

   case class WeatherLeg()

   type Representations = String

   type MainPublication = String

   case class ProtectionTerms()

   type ReferenceLevelUnit = String

   case class ReferenceType()

   case class LegIdentifier()

   case class FxDisruption()

   type SettlementPeriodLeverage = String

   type IdentifiedPayerReceiver = String

   case class CommodityVarianceCalculation()

   case class BasketReferenceInformation()

   type InstrumentTradeQuantity = String

   type PGPDataType = String

   case class IndependentAmount()

   case class PaymentRule()

   type Rate = String

   case class SpreadSchedule()

   type GenericOptionStrike = String

   case class VarianceLeg()

   type ExerciseNotice = String

   case class CommodityBarrier()

   case class InterestLegCalculationPeriodDates()

   type PartyNoticePeriod = String

   type FxStrikeReference = String

   case class MandatoryEarlyTermination()

   case class DateRelativeToPaymentDates()

   type CommodityPayRelativeToEvent = String

   case class AdjustableDate2()

   type ValuationPostponement = String

   type ResetFrequency = String

   type CorporateActionType = String

   type ObjectType = String

   type Strike = String

   case class InstrumentTradePricing()

   case class Commodity()

   type IssuerId = String

   type CollateralProfile = String

   case class WeatherCalculationPeriods()

   type DateTimeList = String

   type PointValue = String

   case class SequencedDisruptionFallback()

   case class PassThroughItem()

   type FxStrike = String

   type UnderlyerLoanRate = String

   type LegalEntityReference = String

   type WeatherStationWMO = String

   case class FxAccrualSettlementPeriodSchedule()

   type WeightedAveragingObservation = String

   type FxSwapLeg = String

   case class LegalEntity()

   case class FloatingAmountEvents()

   case class CalculationPeriodAmount()

   case class ReferencePair()

   type CreditSupportAgreementIdentifier = String

   type DeliveryNearby = String

   case class OnBehalfOf()

   type CommodityCalculationPeriodsSchedule = String

   type DigestValueType = String

   type PersonRole = String

   case class SettlementInstruction()

   type ActionType = String

   case class StubCalculationPeriod()

   case class Asset()

   case class FloatingLegCalculation()

   type Lag = String

   type AssetClass = String

   type MarketDisruption = String

   type Discounting = String

   case class FxRateObservable()

   type DateRange = String

   type IndexAnnexSource = String

   case class RepoNearLeg()

   type FloatingStrikePrice = String

   case class IndexReferenceInformation()

   type PartyRoleType = String

   type ReferenceAmount = String

   case class Frequency()

   type HourMinuteTime = String

   type CommodityFrequencyType = String

   type CommodityStrikeSchedule = String

   type FixedPrice = String

   type GenericExerciseStyle = String

   type ActionOnExpiration = String

   case class PassThrough()

   type ContractualSupplement = String

   type IndexId = String

   case class SingleUnderlyer()

   type InitialMarginCalculation = String

   type Time = String

   case class OptionalEarlyTermination()

   case class FxAccrualRegionLowerBound()

   type UnderlyerReference = String

   type Period = String

   case class CalculationPeriodDates()

   type FxLinkedNotionalSchedule = String

   type PersonReference = String

   type TimezoneLocation = String

   type ReportingPurpose = String

   type Lien = String

   type ApprovalType = String

   type AdjustableDatesOrRelativeDateOffset = String

   type CommodityReturnCalculation = String

   type DeterminationMethod = String

   type FxEuropeanExercise = String

   case class FxTargetSettlementPeriodPayoff()

   type PrePayment = String

   type OrganizationType = String

   type DividendPeriodDividend = String

   case class UnderlyerInterestLeg()

   type WeatherStationAirport = String

   type TerminatingEvent = String

   type DeliveryMethod = String

   type FixedPaymentAmount = String

   case class ChangeEvent()

   type CommodityForward = String

   type BasketName = String

   type FxPerformanceFixedLeg = String

   type Beneficiary = String

   case class TradeChangeContent()

   type SPKIDataType = String

   type Trader = String

   case class EndUserExceptionDeclaration()

   type ReferenceBankId = String

   type WeatherCalculationPeriod = String

   case class ContactInformation()

   type InterestRateStreamReference = String

   case class TriParty()

   case class FxAccrual()

   type CashflowId = String

   case class Notional()

   type WeatherLegCalculation = String

   type BusinessUnitReference = String

   case class PriceSourceDisruption()

   type RoutingId = String

   type CommoditySwapLeg = String

   case class FinalCalculationPeriodDateAdjustment()

   case class RelatedBusinessUnit()

   case class Formula()

   type ImplementationSpecificationVersion = String

   type PaymentReference = String

   type AdjustableOrRelativeDates = String

   type LoanParticipation = String

   type AmountSchedule = String

   type RegulatorId = String

   case class ReferenceInformation()

   case class StrikeSpread()

   type OriginatingEvent = String

   case class RateObservation()

   case class FxOptionPremium()

   case class PaymentDetails()

   case class FxDisruptionProvisions()

   type CommodityBase = String

   case class VarianceAmount()

   case class PartyTradeInformation()

   type DeterminationMethodReference = String

   case class PackageInformation()

   case class AdjustableRelativeOrPeriodicDates2()

   type TelephoneNumber = String

   case class ManualExercise()

   case class PrincipalExchangeDescriptions()

   case class BusinessEventIdentifier()

   type ConvertibleBond = String

   type FxStraddlePremium = String

   type QuantityReference = String

   case class CommodityRelativePaymentDates()

   case class FxDisruptionEvents()

   type FxPayoffCap = String

   type PricingModel = String

   type DayCountFraction = String

   type SpreadScheduleReference = String

   case class GenericCommodityDeliveryPeriod()

   type ClearanceSystem = String

   type EntityType = String

   case class CreditEventNotice()

   type CouponType = String

   case class Approvals()

   type Reference = String

   type CommoditySettlementPeriodsNotionalQuantity = String

   case class FxConversion()

   type FxDisruptionFallback = String

   case class Swap()

   type ProductId = String

   type TradeCategory = String

   case class CreditEvents()

   type CalculationPeriodDatesReference = String

   type EquityAsset = String

   case class Volatility()

   type MimeType = String

   case class TradeNotionalChange()

   case class Restructuring()

   type QuotedCurrencyPair = String

   type CalculationAgent = String

   type FxInformationSource = String

   case class CommodityBasketByPercentage()

   case class CommodityBasketUnderlyingByNotional()

   case class EarlyTerminationProvision()

   type SettlementPeriodFixingDates = String

   type FxLinkedNotionalAmount = String

   type ExerciseFee = String

   case class TradeAmendmentContent()

   type FxLevelReference = String

   case class MasterConfirmation()

   type FxTargetReference = String

   type DualCurrencyStrikePrice = String

   type ExchangeId = String

   type CorrelationId = String

   case class ImplementationSpecification()

   case class ExchangeTradedContract()

   type InitialPayment = String

   case class DeliverableObligations()

   case class TermDepositFeatures()

   type EventId = String

   type ScheduleReference = String

   type CommoditySpreadSchedule = String

   case class CancelableProvision()

   type MasterAgreementVersion = String

   case class ReferenceBank()

   case class CommodityMultipleExercise()

   type Routing = String

   case class PrevailingTime()

   type CommodityPremium = String

   type AdjustedPaymentDates = String

   type CollateralizationType = String

   type AccountType = String

   type MasterAgreementId = String

   type StreetAddress = String

   type CashflowNotional = String

   case class GeneralTerms()

   case class CommodityBasketByNotional()

   type SettlementMethod = String

   case class FxAccrualSettlementPeriod()

   type BusinessUnit = String

   type MessageAddress = String

   case class TradePackage()

   case class TradeLegSizeChange()

   type ConfirmationMethod = String

   case class StrategyComponentIdentification()

   case class StubCalculationPeriodAmount()

   type InformationProvider = String

   case class SettlementProvision()

   case class SplitSettlement()

   type PartyTradeIdentifierReference = String

   case class TradeHeader()

   case class Trade()

   case class Price()

   type Date = String

   type SettlementRateOption = String

   case class Variance()

   type MakeWholeProvisions = String

   type MultipleValuationDates = String

   case class FixedAmountCalculation()

   case class Offset()

   case class NonDeliverableSettlement()

   type CutName = String

   type SupervisoryBody = String

   type ApprovalId = String

   type LagReference = String

   case class CommodityRelativeExpirationDates()

   case class FxStraddle()

   type RoutingIdsAndExplicitDetails = String

   type CommoditySwap = String

   type TimestampTypeScheme = String

   type GenericResetFrequency = String

   type Basket = String

   type CashflowType = String

   case class FxTargetLeverage()

   case class StubValue()

   case class PaymentDates()

   type ValuationDatesReference = String

   case class AveragingObservationList()

   type SettlementPeriodsReference = String

   case class FxRateSourceFixing()

   case class X509IssuerSerialType()

   type NonNegativeInteger = String

   case class AdjustableDate()

   type ExchangeRate = String

   type CommodityBasketUnderlyingByPercentage = String

   case class RequestMessageHeader()

   type PrincipalExchanges = String

   case class FxFeature()

   type SpecifiedCurrency = String

   case class Barrier()

   case class CommodityInformationSource()

   type NotionalStepRule = String

   case class ReferencePool()

   type ExercisePeriod = String

   case class Loan()

   type Stub = String

   case class PackageHeader()

   type AdjustableDates = String

   case class FxTargetRebate()

   type Knock = String

   type AssetPool = String

   type FxTargetConstantPayoff = String

   type InterestAccrualsMethod = String

   type SpreadScheduleType = String

   type PhysicalSettlementPeriod = String

   case class ClearingInstructions()

   type NormalizedString = String

   type ProductType = String

   type ResourceId = String

   type ExecutionType = String

   type CommodityTrigger = String

   case class TradeUnderlyer2()

   case class FxTargetRegionLowerBound()

   case class NonNegativeAmountSchedule()

   type CreditSeniority = String

   type PartyName = String

   type RelevantUnderlyingDateReference = String

   case class DividendLeg()

   case class VolatilityLeg()

   case class RelatedParty()

   case class Party()

   type ExecutionDateTime = String

   case class CommodityFixedInterestCalculation()

   case class BusinessDayAdjustments()

   type FixedRate = String

   type SinglePartyOption = String

   case class PaymentDetail()

   type PersonId = String

   type IndustryClassification = String

   type RestructuringType = String

   case class EquityExerciseValuationSettlement()

   type FxPivotReference = String

   type FxExpiryDate = String

   type RateSourcePage = String

   type AutomaticExercise = String

   type PartyEntityClassification = String

   type RequestedClearingAction = String

   case class FacilityExecutionExceptionDeclaration()

   case class PartyTradeIdentifier()

   case class RelatedPerson()

   case class Compounding()

   case class FxTargetPhysicalSettlement()

   case class DateRelativeToCalculationPeriodDates()

   case class Underlyer()

   type EquityCorporateEvents = String

   type SettlementPriceSource = String

   type ResetDatesReference = String

   case class ReturnSwapAdditionalPayment()

   type WeatherIndex = String

   type FxAdjustedDateAndDateAdjustments = String

   case class ResetDates()

   case class PeriodicDates()

   type EquityMultipleExercise = String

   type ClearingStatusValue = String

   case class CommodityFx()

   case class YieldCurveMethod()

   case class InterestRateStream()

   type ProductReference = String

   case class ContractualTermsSupplement()

   case class BusinessCenters()

   case class BasketConstituent()

   case class RoutingIds()

   case class AdjustableRelativeOrPeriodicDates()

   case class VersionedTradeId()

   type BusinessDayAdjustmentsReference = String

   type ObservationFrequency = String

   case class PhysicalSettlementTerms()

   case class ExerciseFeeSchedule()

   type CreditSupportAgreementType = String

   type CommodityInformationProvider = String

   case class WeatherIndexData()

   case class FxCashSettlement()

   type DigestMethodType = String

   type HexBinary = String

   type PositiveMoney = String

   type PriceQuoteUnits = String

   type AssetReference = String

   case class Allocations()

   type PackageSummary = String

   type Rounding = String

   type CalculationPeriodsReference = String

   case class ReturnSwapNotional()

   type Tranche = String

   type CommodityAmericanExercise = String

   type SingleValuationDate = String

   case class PaymentCalculationPeriod()

   type RequiredIdentifierDate = String

   type HMACOutputLengthType = String

   type FxSettlementSchedule = String

   case class AveragingPeriod()

   case class ExtendibleProvisionAdjustedDates()

   type FxFixingScheduleSimple = String

   case class GenericProductExchangeRate()

   type NumberOfUnitsReference = String

   type AdditionalFixedPayments = String

   type AdditionalDisruptionEvents = String

   case class ConstituentWeight()

   case class Documentation()

   case class KeyValueType()

   case class PartyMessageInformation()

   case class GrossCashflow()

   case class CommoditySwaptionUnderlying()

   type BasicQuotation = String

   case class QuotationCharacteristics()

   case class CollateralValuation()

   case class InitialMargin()

   case class InstrumentTradePrincipal()

   case class ReferenceObligation()

   type FloatingAmountProvisions = String

   type QuoteTiming = String

   type PartyReference = String

   case class SettlementInformation()

   type FxFixingSchedule = String

   case class Trigger()

   case class CommodityExercise()

   type MessageId = String

   type EntityId = String

   case class SignatureMethodType()

   type FloatingRateCalculationReference = String

   type AssetMeasureType = String

   type ResourceLength = String

   case class FxAccrualSettlementPeriodPayoff()

   type IndexName = String

   type RetrievalMethodType = String

   type VolatilityCap = String

   type ContractualDefinitions = String

   case class OptionExpiry()

   type NonPeriodicFixedPriceLeg = String

   case class DirectionalLeg()

   type NotionalAmount = String

   case class ReferencePoolItem()

   case class TransformType()

   type RateReference = String

   type CommodityExpireRelativeToEvent = String

   type AveragingSchedule = String

   type FxAverageStrike = String

   type InstrumentId = String

   type FxComplexBarrierBaseReference = String

   case class FeeLeg()

   type GenericProductQuotedCurrencyPair = String

   type FxPerformanceFloatingLeg = String

   case class OptionalEarlyTerminationAdjustedDates()

   case class SupervisorRegistration()

   type BusinessCenter = String

   type CalculationPeriodFrequency = String

   type RelativeDates = String

   case class RelativeDateOffset()

   type IdentifiedRate = String

   case class OrderIdentifier()

   type TradeId = String

   case class TriggerEvent()

   case class BusinessCenterTime()

   type FxValuationDateOffset = String

   type NumberOfOptionsReference = String

   type FxCashSettlementSimple = String

   type ProtectionTermsReference = String

   case class Payment()

   type UnderlyingAssetTranche = String

   case class TradeNovationContent()

   type MatrixType = String

   type FxForwardStrikePrice = String

   type FxLevel = String

   type OptionExerciseAmounts = String

   type SettlementRateSource = String

   case class CorrespondentInformation()

   type PackageType = String

   type Decimal = String

   case class EquityAmericanExercise()

   case class TradeLegPriceChange()

   type EquityEuropeanExercise = String

   type MasterAgreementType = String

   type SwaptionPhysicalSettlement = String

   type FxAverageRate = String

   case class FloatingRateCalculation()

   type ReportingRegimeName = String

   type FxTargetPayoffRegionReference = String

   type Initial = String

   type CommodityNotionalAmount = String

   type MatrixTerm = String

   case class Bond()

   case class CommodityPerformanceSwapLeg()

   case class FxAccrualLinearPayoffRegion()

   type FxAccrualRegionUpperBound = String

   type FxExchangedCurrency = String

   type OrderId = String

   case class AdjustableDateOrRelativeDateSequence()

   type RepoFarLeg = String

   type FxTargetRegionUpperBound = String

   case class X509DataType()

   type GracePeriodExtension = String

   type BondReference = String

   type PartyGroupType = String

   type AmountReference = String

   case class SwaptionAdjustedDates()

   type CancellationEvent = String

   type AdjustedRelativeDateOffset = String

   

   type EntityClassification = String

   type Currency = String

   type CrossRate = String

   type CountryCode = String

   type Base64Binary = String

   type CommodityQuantityFrequency = String

   type NetAndGross = String

   case class ReportingRegime()

   type AdditionalTerm = String

   type PrincipalExchangeAmount = String

   type BoundedVariance = String

   case class TradeIdentifier()

   type IdentifiedCurrencyReference = String

   case class MultipleExercise()

   type WeatherStationWBAN = String

   case class MasterAgreement()

   case class DividendPayout()

   case class StrategyFeature()

   type LegId = String

   type FxScheduleReference = String

   type FxAccrualBarrier = String

   type EarlyTerminationEvent = String

   type FloatingRate = String

   case class Person()

   case class Obligations()

   type Validation = String

   type CreditDocument = String

   case class LegAmount()

   case class VolatilityAmount()

   type FxDisruptionEvent = String

   type CommodityValuationDates = String

   case class InterestCalculation()

   type FloatingRateIndex = String

   type PhysicalSettlement = String

   case class FxRate()

   type FxFlexibleForwardExecutionPeriod = String

   type AccountId = String

   type Language = String

   type GYearMonth = String

   type GoverningLaw = String

   case class FxAccrualRegion()

   type Math = String

   case class CashSettlementTerms()

   type FxAccrualPayoffRegionReference = String

   case class CommodityNotionalQuantitySchedule()

   type FxFixing = String

   type AdditionalPaymentAmount = String

   case class Commission()

   type OrganizationCharacteristic = String

   type BasketId = String

   type IntermediaryInformation = String

   type PartialExercise = String

   type CommodityExercisePeriods = String

   type NonNegativePayment = String

   type FxKnockoutCount = String

   type DateReference = String

   type InterestShortFall = String

   case class CommodityNotionalQuantity()

   type InterpolationMethod = String

   type DateOffset = String

   case class CashSettlement()

   type Token = String

   type FacilityType = String

   type DataProvider = String

   type CommoditySpread = String

   type Unit = String

   type NotifyingParty = String

   case class CommodityMarketDisruption()

   case class NonNegativeMoney()

   case class FloatingAmountCalculation()

   type CreditDerivativesNotices = String

   case class Resource()

   type CalendarSpread = String

   case class FxExpirySchedule()

   case class FxDateOffset()

   type DisruptionFallback = String

   case class Money()

   type PartyId = String

   type DateList = String

   type FxFixingObservation = String

   case class ProductComponentIdentifier()

   type InterconnectionPoint = String

   type ExecutionVenueType = String

   type AdjustableOffset = String

   type CommodityPhysicalAmericanExercise = String

   type PaymentType = String

   type OptionType = String

   case class Schedule()

   type FxAccrualAverageStrikeReference = String

   type MortgageSector = String

   type MasterConfirmationType = String

   case class PeriodicPayment()

   type EntityName = String

   type IndexAdjustmentEvents = String

   case class WeatherStation()

   case class CreditSupportAgreement()

   type ExtensionEvent = String

   case class TradeProcessingTimestamps()

   type PaymentDatesReference = String

   type CommodityEuropeanExercise = String

   case class Exercise()

   type GenericFrequency = String

   type FxRateObservableReference = String

   case class RoutingExplicitDetails()

   type MatrixSource = String

   case class Collateral()

   type StrikeSchedule = String

   type SinglePayment = String

   case class FxDisruptionFallbacks()

   type CreditEventsReference = String

   type Quanto = String

   type ReportingRole = String

   case class ExerciseProcedure()

   case class InterestLegResetDates()

   type PortfolioName = String

   type IdentifiedCurrency = String

   type AllocationReportingStatus = String

   case class AveragePriceLeg()

   type RestrictedPercentage = String

   case class Asian()

   case class CommodityPhysicalExercise()

   type CanonicalizationMethodType = String

   type BusinessUnitRole = String

   case class PrincipalExchangeFeatures()

   type SignatureValueType = String

   type VerificationMethod = String

   type FxAccrualTriggerReference = String

   case class CommodityBasket()

   case class FxTargetBarrier()

   case class FloatingRateDefinition()

   case class EquityBermudaExercise()

   type IdentifiedDate = String

   type FxFixingDate = String

   type AccountReference = String

   type NotionalAmountReference = String

   type Integer = String

   case class Approval()

   type CreditRating = String

   type Empty = String

   type LinkId = String

   case class FxTargetSettlementPeriodSchedule()

   case class CashPriceMethod()

   type Boolean = String

   type CalculationAmount = String

   case class Account()

   case class CashSettlementReferenceBanks()

   type FutureValueAmount = String

   case class ReferenceLevel()

   type FxAccrualStrikeReference = String

   type ReportingCurrencyType = String

   type AccountName = String

   type ClearingExceptionReason = String

   case class TradeTimestamp()

   case class Product()

   type Step = String

   case class TradeReferenceInformation()

   type NonNegativeStep = String

   case class Mortgage()

   type FirstPeriodStartDate = String

   type SimplePayment = String

   case class EquityValuation()

   type FxSpotRateSource = String

   case class ValuationDate()

   type PCDeliverableObligationCharac = String

   type CommodityNotionalAmountReference = String

   type FixedPaymentLeg = String

   type InterestLegCalculationPeriodDatesReference = String

   type GenericCommodityGrade = String

   case class KeyInfoType()

   type AdjustableOrAdjustedDate = String

   type ExerciseProcedureOption = String

   type CryptoBinary = String

   case class DualCurrencyFeature()

   type RelativePrice = String

   case class AdjustableOrRelativeDate()

   type FxTemplateTerms = String

   type SettlementPeriods = String

   type ResourceType = String

   type TransactionCharacteristic = String

   case class FxTargetLinearPayoffRegion()

   type CalculationPeriodsDatesReference = String

   type PrincipalExchange = String

   type ReturnSwapNotionalAmountReference = String

   type MarketDisruptionEvent = String

   type PendingPayment = String

   case class FallbackReferencePrice()
}