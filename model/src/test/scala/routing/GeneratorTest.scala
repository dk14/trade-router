package routing

import java.io.File

import org.apache.commons.io.FileUtils
import org.scalatest.FunSuite
import org.apache.xerces.xs._
import org.apache.xerces.impl.xs.XMLSchemaLoader

import scala.io.Source

/**
 * Created by user on 11/16/15.
 */
class GeneratorTest extends FunSuite {
  val loader = new XMLSchemaLoader()

  test("fpml"){
    val model = loader.loadURI(getClass.getClassLoader.getResource("confirmation/fpml-main-5-9.xsd").toURI.toString)
    val gen = new ModelGeneratorImpl(model)

    //println("generating...")
    val data = gen.getMeta("executionAdvice").map("   " + _).mkString("\n\n").replaceAll("type String = String", "")

    FileUtils.write(new File(s"./model/src/main/scala/model/ExecAdvice.scala"), "package model\n\n object execAdvice{\n" +data + "\n}")

  }

}
